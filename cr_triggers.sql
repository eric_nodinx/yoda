CREATE TRIGGER order_update AFTER UPDATE ON orders 
FOR EACH ROW
	BEGIN
		INSERT INTO audit_trail VALUES ( OLD.level, NEW.level, CURRENT_TIMESTAMP );
	END;
