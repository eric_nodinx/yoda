#!/bin/bash
#create a full backup using a blocking call to ensure data integrity
#test version 1.0 change path for production
filedate=`date "+%m-%d-%Y:%H:%M"`
sqlite3 devdata.db .dump > ../backups/devdata-$filedate.sql

