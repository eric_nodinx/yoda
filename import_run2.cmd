.mode list
.import "../etl/orders.csv" orders 
.import "../etl/customers.csv" customers 
.import "../etl/markers.csv" mrkrequest 
.import "../etl/details.csv" orderdetails 
.import "../etl/items.csv" items 
.import "../etl/proofs.csv" proofs
.import "../etl/addresses.csv" addresses

.quit
