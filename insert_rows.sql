INSERT INTO proofs (id, proof_path, change_dtim, change_user) VALUES (1, '/img/proof_tmp.png', '', '');

INSERT INTO yesno (id, type) VALUES (1, 'Yes'), (2, 'No');

INSERT INTO newrev (id, type) VALUES (1, 'New'), (2, 'Revision');

INSERT INTO shippkup (id, type) VALUES (1, 'Ship'), (2, 'Pickup');

INSERT INTO configs (id, name, desc, value) values (1, 'SENDGRID_API_KEY', 'send grid api key', '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');

INSERT INTO resourcetypes (id, type) VALUES (1, 'Internal'), (2, 'External');

INSERT INTO inbndinvdescs (id, type) VALUES (1, 'Ordered'), (2, 'Waiting'), (3, 'Received');

INSERT INTO items (id, v_item, color, desc, type) VALUES (1, 'Business Cards', '', 'Business Cards', 1), (2, 'Post Cards', '', 'Post Cards', 2), (3, 'Brochures', '', 'Brochures', 3), (4, 'Flyers', '', 'Flyers', 4), (5, 'Posters', '', 'Posters', 5), (6, 'A-Frame Signs', '', 'A-Frame Sign', 6), (7, 'Banners', '', 'Banners', 7), (8, 'Retractable Banner', '', 'Reatractable Banner', 8), (9, 'Sign', '', 'Rigid Substrate Sign', 9), (10, 'Yard Sign', '', 'Yard Sign', 10), (11, 'Table Runner', '','Table Runner', 11), (12, 'Table Throws', '', 'Table Throws', 12), (13, 'Flag Banners', '', 'Flag Banners', 13), (14, 'Canopy Tents', '', 'Canopy', 14), (15, 'Printed Vinyl Decals', '', 'Printed Vinyl Decals', 15), (16, 'Cut Vinyl Decal', '', 'Cut Vinyl Decal', 16), (17, 'Static Cling Decals', '', 'Static Cling Decals', 17), (18, 'Magnetic Decal', '', 'Magnetic Decal', 18), (19, 'Applied Vinyl Graphics/Wraps', '', 'Applied Vinyl Graphics/Wraps', 19), (20, 'Magnetic Graphics', '', 'Magnetic Graphics', 20), (21, 'Floor Graphics', '', 'Floor Graphics', 21);

INSERT INTO addresses (id, desc, cust_id, street1, street2, city, state, province, country, zipcode) VALUES (1, "Bill To", 10, "111 Oak St", "", "Albany", "OR", "", "USA", "97321");

INSERT INTO projects (id, status_id, rep_id, proj_due, name) VALUES (1, 1, 1, "12/15/2016", "Test");

INSERT INTO projectstats (id, proj_stat) VALUES (1, "New"), (2, "In Progress"), (3, "Pending"), (4, "Done");

INSERT INTO approves (id, name) VALUES (1,"Corey"), (2,"Steph"), (3,"Mark"), (4,"Nina"), (5,"Nathan"), (6,"Brady"), (7,"Danny"), (8,"Travis"), (9,"Trent"), (10,"Tyler"), (11,"Mike"), (12,"Steven"), (13,"Briana"), (14,"Chad"), (15,"Andrew"), (16,"Joy"), (17,"Jesse"), (18,"Amber");

INSERT INTO reps (id, name, user_id) VALUES (1,"None",1),(2,"Corey",17), (3,"Steph",17), (4,"Mark",19), (5,"Nina",3), (6,"Nathan",4), (7,"Joy",14), (8,"Amber",16), (9,"Trent",7);

INSERT INTO artists (id, name, user_id) VALUES (1,"None", 2),(2,"Mike",11), (3,"Steven",10), (4,"Alison",18);

.quit
