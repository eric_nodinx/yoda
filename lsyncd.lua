settings {
	logfile = "/var/log/lsyncd/lsyncd.log",
	statusFile = "/var/log/lsyncd/lsyncd-status.log",
	statusInterval = 20
}
sync {
	default.rsync,
	source="/Users/ed/Desktop/yoda/nodinxprd/public/res",
	target="nodinx@rsync.keycdn.com:zones/yoda/",
	rsync = {
	binary = "/usr/local/bin/rsync",
	compress = true,
	acls = true,
	verbose = true,
	_extra = { "--chmod=u=rwX,g=rX" },
	}
}
