# -*- coding: utf-8 -*-
"""job_list controller module"""

from tg import expose, flash, require, url, lurl
from tg import predicates
import tw2.core as twc
from tw2.forms.widgets import Form, BaseLayout, TextField, TextArea, SubmitButton
from tw2.forms import DataGrid

class jobsListForm(Form):
  class child(BaseLayout):
    # Uncomment this line if your controller requires an authenticated user
    # allow_only = predicates.not_anonymous()

    template = ''' 
	<tr class="jobs-table-row hidden-xs">
		<td class="customer">
			${w.cust_name}
		</td>
		<td class="job">
			<a href="${tg.url('/job/')}">3 Sheets T-Shirts</a>
		</td>
		<td class="job-number">
			${w.job_num}
		</td>
		<td class="order-type">
			${w.order_type}	
		</td>
		<td class="order-date">
			${w.order_date}	
		</td>
		<td class="processing-date">
			${w.proc_date}	
		</td>
		<td class="job-due-date">
			9/29/16 (H)
		</td>
		<td class="account-rep">
			mark
		</td>
		<td class="scheduled">
			x
		</td>
		<td class="ordered">
			x
		</td>
		<td class="art">
			mike
		</td>
		<td class="counted">
			x
		</td>
		<td class="burned">
			x
		</td>
		<td class="invoice">
			Briana
		</td>
		<td class="order-status">
			Order Written
		</td>
		<td class="marker-status">
			Marker Finished
		</td>
		<td class="last-edit">
			x
		</td>
	</tr>
	<tr class="jobs-table-row visible-xs">
		<td class="customer">
			<table style="width: 100%;">
				<tbody>
					<tr>
						<td colspan="3">
							<h5>3 Sheets Brewery</h5>
							<h4><a href="/pages/job.html">3 Sheets T-Shirts</a></h4>
						</td>
					</tr>
					<tr>
						<td colspan="1"><label>Art:</label> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>
						<td colspan="1"><label>Print:</label> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>
						<td colspan="1"><label>Ship:</label> 11/1</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="progress">
								<div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"><span>60% (Production: Boxing Now)</span></div>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<button type="button" class="btn btn-lg btn-primary" style="width: 100%;">Expand</button>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>

    '''
    
