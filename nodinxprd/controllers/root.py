# -*- coding: utf-8 -*-
"""Main Controller"""

from tg import expose, flash, require, url, lurl
from tg import request, redirect, require, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.predicates import not_anonymous
from tg.exceptions import HTTPFound
from tg import predicates
from tgext.crud import EasyCrudRestController
from nodinxprd import model
from decimal import *
from datetime import date, timedelta, datetime
from nodinxprd.model.order import YesNo, ShipPkup, Vendor, Address, Customer, Order, OrderDetail, PO, PODetail, MarkerRequest, MarkerRequestLog, Category, OrderOption, OptionType, Proof, OrderStatus, ShipType, OrderType, Item, Priority, MarkerStatus, InbndInvDesc, PrintTime, Project, ProjectStat, Rep, NewRev, Artist, Config, Resource, ItemType, ItemOption, Worksheet, WorksheetDetail, EmailStatus, EmailTemplate, EmailQueue, EmailLog
from nodinxprd.model.auth import User
from nodinxprd.controllers.secure import SecureController
from nodinxprd.controllers.jobs_list import jobsListForm
from nodinxprd.model import DBSession
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController
import time
import os
from time import strftime
from nodinxprd.lib.base import BaseController
from nodinxprd.controllers.error import ErrorController
from tgext.crud import CrudRestController
from sprox.tablebase import TableBase
from sprox.formbase import EditableForm, AddRecordForm
from sprox.fillerbase import TableFiller, EditFormFiller
import tw2.core as twc
import tw2.forms as twf
from tw2.forms import DataGrid
from tg.decorators import paginate
from tw2.forms.datagrid import Column
import tw2.sqla, tw2.dynforms
from markupsafe import Markup
from tw2.jqplugins.chosen import ChosenSingleSelectField as SingleSelectField
from sqlalchemy import asc, desc, func
from dateutil.parser import parse
#from tgext.mailer import Message
#from tgext.mailer import get_mailer
import sendgrid
#from sendgrid.helpers.mail import *
from tw2 import tinymce
import operator
import logging
log = logging.getLogger(__name__)

class DeclarativeItemTypeController(EasyCrudRestController):
    model = ItemType

    class new_form_type(AddRecordForm):
        __model__ = ItemType
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = ItemType
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = ItemType

    class table_type(TableBase):
        __model__ = ItemType

    class table_filler_type(TableFiller):
        __model__ = ItemType

class DeclarativeItemController(EasyCrudRestController):
    model = Item

    class new_form_type(AddRecordForm):
        __model__ = Item
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = Item
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = Item

    class table_type(TableBase):
        __model__ = Item

    class table_filler_type(TableFiller):
        __model__ = Item

class DeclarativeProjectController(EasyCrudRestController):
    model = Project

    class new_form_type(AddRecordForm):
        __model__ = Project
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = Project
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = Project

    class table_type(TableBase):
        __model__ = Project

    class table_filler_type(TableFiller):
        __model__ = Project

class DeclarativeMarkerStatusController(EasyCrudRestController):
    model = MarkerStatus

    class new_form_type(AddRecordForm):
        __model__ = MarkerStatus
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = MarkerStatus
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = MarkerStatus

    class table_type(TableBase):
        __model__ = MarkerStatus

    class table_filler_type(TableFiller):
        __model__ = MarkerStatus

class DeclarativePriorityController(EasyCrudRestController):
    model = Priority

    class new_form_type(AddRecordForm):
        __model__ = Priority
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = Priority
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = Priority

    class table_type(TableBase):
        __model__ = Priority

    class table_filler_type(TableFiller):
        __model__ = Priority


class DeclarativePODetailController(EasyCrudRestController):
    model = PODetail

    class new_form_type(AddRecordForm):
        __model__ = PODetail
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = PODetail
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = PODetail

    class table_type(TableBase):
        __model__ = PODetail

    class table_filler_type(TableFiller):
        __model__ = PODetail

class DeclarativePOController(EasyCrudRestController):
    model = PO

    class new_form_type(AddRecordForm):
        __model__ = PO
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = PO
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = PO

    class table_type(TableBase):
        __model__ = PO

    class table_filler_type(TableFiller):
        __model__ = PO

class DeclarativeOrderOptionController(EasyCrudRestController):
    model = OrderOption

    class new_form_type(AddRecordForm):
        __model__ = OrderOption
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = OrderOption
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = OrderOption

    class table_type(TableBase):
        __model__ = OrderOption

    class table_filler_type(TableFiller):
        __model__ = OrderOption

class DeclarativeCategoryController(EasyCrudRestController):
    model = Category

    class new_form_type(AddRecordForm):
        __model__ = Category
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = Category
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = Category

    class table_type(TableBase):
        __model__ = Category

    class table_filler_type(TableFiller):
        __model__ = Category

class DeclarativeOrderStatusController(EasyCrudRestController):
    model = OrderStatus

    class new_form_type(AddRecordForm):
        __model__ = OrderStatus
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = OrderStatus
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = OrderStatus

    class table_type(TableBase):
        __model__ = OrderStatus

    class table_filler_type(TableFiller):
        __model__ = OrderStatus

class DeclarativeOrderTypeController(EasyCrudRestController):
    model = OrderType

    class new_form_type(AddRecordForm):
        __model__ = OrderType
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = OrderType
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = OrderType

    class table_type(TableBase):
        __model__ = OrderType

    class table_filler_type(TableFiller):
        __model__ = OrderType

class DeclarativeShipTypeController(EasyCrudRestController):
    model = ShipType

    class new_form_type(AddRecordForm):
        __model__ = ShipType
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = ShipType
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = ShipType

    class table_type(TableBase):
        __model__ = ShipType

    class table_filler_type(TableFiller):
        __model__ = ShipType

class DeclarativeOrderStatusController(EasyCrudRestController):
    model = OrderStatus

    class new_form_type(AddRecordForm):
        __model__ = OrderStatus
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = OrderStatus
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = OrderStatus

    class table_type(TableBase):
        __model__ = OrderStatus

    class table_filler_type(TableFiller):
        __model__ = OrderStatus

class DeclarativeProofController(EasyCrudRestController):
    model = Proof

    class new_form_type(AddRecordForm):
        __model__ = Proof
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = Proof
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = Proof

    class table_type(TableBase):
        __model__ = Proof

    class table_filler_type(TableFiller):
        __model__ = Proof

class DeclarativeMarkerRequestController(EasyCrudRestController):
    model = MarkerRequest

    class new_form_type(AddRecordForm):
        __model__ = MarkerRequest
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = MarkerRequest
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = MarkerRequest

    class table_type(TableBase):
        __model__ = MarkerRequest

    class table_filler_type(TableFiller):
        __model__ = MarkerRequest

class DeclarativeMarkerRequestLogController(EasyCrudRestController):
    model = MarkerRequestLog

    class new_form_type(AddRecordForm):
        __model__ = MarkerRequestLog
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = MarkerRequestLog
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = MarkerRequestLog

    class table_type(TableBase):
        __model__ = MarkerRequestLog

    class table_filler_type(TableFiller):
        __model__ = MarkerRequestLog

class DeclarativeVendorController(EasyCrudRestController):
    model = Vendor

    class new_form_type(AddRecordForm):
        __model__ = Vendor
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = Vendor
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = Vendor

    class table_type(TableBase):
        __model__ = Vendor

    class table_filler_type(TableFiller):
        __model__ = Vendor

class DeclarativeCustomerController(EasyCrudRestController):
    model = Customer

    class new_form_type(AddRecordForm):
        __model__ = Customer
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = Customer
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = Customer

    class table_type(TableBase):
        __model__ = Customer

    class table_filler_type(TableFiller):
        __model__ = Customer

class DeclarativeOrderDetailController(EasyCrudRestController):
    model = OrderDetail

    class new_form_type(AddRecordForm):
        __model__ = OrderDetail
        __omit_fields__ = ['id']

    class edit_form_type(EditableForm):
        __model__ = OrderDetail
        __omit_fields__ = ['id']

    class edit_filler_type(EditFormFiller):
        __model__ = OrderDetail

    class table_type(TableBase):
        __model__ = OrderDetail

    class table_filler_type(TableFiller):
        __model__ = OrderDetail

class DeclarativeOrderController(EasyCrudRestController):
    model = Order

    class new_form_type(AddRecordForm):
        __model__ = Order
        __omit_fields__ = ['customer_id', 'order_id']

    class edit_form_type(EditableForm):
        __model__ = Order
        __omit_fields__ = ['customer_id', 'order_id']

    class edit_filler_type(EditFormFiller):
        __model__ = Order

    class table_type(TableBase):
        __model__ = Order
        __omit_fields__ = ['cust_id', 'order_id', 'prod_apprv', 'edt', 'thnks_sent', 'po_id', 'rush', 'hold', 'cust_apprv', 'job_time_est', 'ship_type', 'int_apprv_schd_date', 'int_apprv_ord_date', 'int_apprv_art_date', 'int_apprv_cntd_date', 'int_apprv_brnd_date', 'int_apprv_invcd_date', 'int_apprv_prdmgr_date', 'int_apprv_prntr_date', 'int_apprv_shpd_date', 'int_apprv_rep_date', 'int_apprv_chkd_date', 'int_apprv_chkd_date']

    class table_filler_type(TableFiller):
        __model__ = Order

class SortableColumn(Column):
    def __init__(self, title, name,  options={}, bg_color=None, job=None):
        super(SortableColumn, self).__init__(name)
        self._title_ = title
        if bg_color:
            self.bg_color = operator.attrgetter(bg_color)
        if job:
            self.job = 'goto(' + operator.attrgetter(id) + ')'
        else:
            self.job = None
        self.options = options
        

    def set_title(self, title):
        self._title_ = title

    def get_title(self):
        current_ordering = request.GET.get('ordercol')
        if current_ordering and current_ordering[1:] == self.name:
           if current_ordering[0] == '+': 
               current_ordering = '-'
           else:
               current_ordering = '+'
        else:
           current_ordering = '+'
 
        current_ordering += self.name

        new_params = dict(request.GET)
        new_params['ordercol'] = current_ordering
 
        new_url = url(request.path_url, params=new_params)
        return Markup(self._title_)
        #return Markup('<a href="%(page_url)s">%(title)s</a>' % dict(page_url=new_url, title=self._title_))

    title = property(get_title, set_title)

class tinyEdit(tinymce.TinyMCEWidget):
    placeholder = "Type something"
    attrs = {}
    mce_options = dict(
        theme = "advanced",
        content_css = "/tinymce.css",
        plugins = "table",
        width = '100%',
        menubar = "table",
        toolbar = "table",
        theme_advanced_toolbar_location = "top",
        theme_advanced_toolbar_align = "left",
        theme_advanced_buttons1 = "tablecontrols, forecolor, backcolor, sizeselect, bold, italic, fontselect, insertdate,inserttime,zoom,bullist,numlist,separator,outdent,indent, separator,undo,redo,separator,hr,removeformat,visualaid,separator,sub,sup,separator,charmap,fontsizeselect",
        theme_advanced_buttons2 = "",
        theme_advanced_buttons3 = "",
        table_styles = "Header 1=header1;Header 2=header2;Header 3=header3",
        table_cell_styles = "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
        table_row_styles = "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
        table_cell_limit = 400,
        table_row_limit = 20,
        table_col_limit = 20
    )

optionsDict = {}

jobGrid = DataGrid(fields=[
    ('Customer', 'Customer.name'),
    ('Job', 'Order.job_name'),
    ('Job #', 'Order.job_num'),
    ('Type', 'OrderType.type'),
    ('Res', 'Order.Resource.type'),
    ('Date', 'Order.due_date'),
    ('Rep', 'Order.Rep.name'),
    ('Schd', lambda obj: Markup('<span ondblclick="setapprv(\'schd\',this.className,$(this),%s)" ontouchstart="setapprv(\'schd\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_schd))),
    ('Ord', lambda  obj: Markup('<span ondblclick="setapprv(\'ord\',this.className,$(this),%s)" ontouchstart="setapprv(\'ord\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_ord))),
    ('Art', lambda  obj: Markup('<span ondblclick="setapprv(\'art\',this.className,$(this),%s)" ontouchstart="setapprv(\'art\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_art))),
    ('Cntd', lambda  obj: Markup('<span ondblclick="setapprv(\'cntd\',this.className,$(this),%s)" ontouchstart="setapprv(\'cntd\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_cntd))),
    ('Brnd', lambda  obj: Markup('<span ondblclick="setapprv(\'brnd\',this.className,$(this),%s)" ontouchstart="setapprv(\'brnd\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_brnd))),
    ('Invcd', lambda  obj: Markup('<span ondblclick="setapprv(\'invcd\',this.className,$(this),%s)" ontouchstart="setapprv(\'invcd\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_invcd))),
    SortableColumn('Status', 'OrderStatus.status', {}, 'OrderStatus.bg_color'),
    ('Marker Status', 'Order.marker.MarkerStatus.mrk_status'),
    ('Inbound', 'Order.inputs')
    ], template='nodinxprd.templates.dashgrid' )

cjobGrid = DataGrid(fields=[('Customer','Order.Customer.name'),
    ('Job Name', 'Order.job_name'),
    ('Job Number', 'Order.job_num'),
    ('Order Type', 'Order.OrderType.type'),
    ('Order Date', 'Order.order_date'),
    ('Due Date', 'Order.due_date'),
    ('Account Rep', 'Order.Rep.name'),
    ('Schd', lambda obj: Markup('<span ondblclick="setapprv(\'schd\',this.className,$(this),%s)" ontouchstart="setapprv(\'schd\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_schd))),
    ('Ord', lambda  obj: Markup('<span ondblclick="setapprv(\'ord\',this.className,$(this),%s)" ontouchstart="setapprv(\'ord\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_ord))),
    ('Art', lambda  obj: Markup('<span ondblclick="setapprv(\'art\',this.className,$(this),%s)" ontouchstart="setapprv(\'art\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_art))),
    ('Cntd', lambda  obj: Markup('<span ondblclick="setapprv(\'cntd\',this.className,$(this),%s)" ontouchstart="setapprv(\'cntd\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_cntd))),
    ('Brnd', lambda  obj: Markup('<span ondblclick="setapprv(\'brnd\',this.className,$(this),%s)" ontouchstart="setapprv(\'brnd\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_brnd))),
    ('Invcd', lambda  obj: Markup('<span ondblclick="setapprv(\'invcd\',this.className,$(this),%s)" ontouchstart="setapprv(\'invcd\',this.className,$(this),%s)" class="%s"></span>' % (obj.Order.id,obj.Order.id,obj.Order.int_apprv_invcd))),
    SortableColumn('Order Status', 'OrderStatus.status', {}, 'OrderStatus.bg_color'),
    ('Marker Status', 'MarkerStatus.mrk_status')
], template='nodinxprd.templates.cjobgrid' )

custGrid = DataGrid(fields=[
           ('Customer','name'), 
           ('AE Num', 'ae_num'), 
           ('Curr Job Num', 'cur_job_num'), 
           ('Contact', 'contact'), 
           ('Email', 'email'), 
           ('Phone', 'phone'), 
           ('Notes', 'notes')], template='nodinxprd.templates.customergrid')

emllogGrid = DataGrid(fields=[
           ('Type','type'), 
           ('Sender', 'sender_id'),
           ('Recipient/s', 'recips'), 
           ('Date Sent', 'sent_date')])

emlTmpltGrid = DataGrid(fields=[
           ('Id','id'), 
           ('Name Num', 'name'), 
           ('Description', 'desc'), 
           ('Subject', 'subject'), 
           ('Offset', 'offset'), 
           ('Active', lambda obj: Markup('<input type="checkbox" tabindex="-1" onchange="settmpltactive($(this), %s)" %s name="active" value="%s"/>' % (obj.id, obj.checked, obj.active)))])

projGrid = DataGrid(fields=[('Project','name'), ('Status', 'prj_stat.proj_stat'), ('Due Date', 'proj_due'), ('Accnt Rep', 'reps.name'), ('Action', lambda obj: Markup('<a href="%s">Remove</a>' % url('/delete/', params=dict(id=obj.id,obj='Project',ret_page='projects',ret_id=''))))], template='nodinxprd.templates.projgrid')

poGrid = DataGrid(fields=[SortableColumn('PO Number','num'), SortableColumn('Order Date', 'order_date'), SortableColumn('Order By', 'order_by'), SortableColumn('Notes', 'notes')], template='nodinxprd.templates.pogrid')

wkshtsGrid = DataGrid(fields=[('PO Number','num'), ('Decsription', 'desc'), ('Notes', 'notes')], template='nodinxprd.templates.wkshtgrid')

inbdGrid = DataGrid(fields=[ SortableColumn('Ship From', 'ship_from'), SortableColumn('Arrival Date', 'inbnd_date'), SortableColumn('Vendor', 'vendor'), SortableColumn('Style', 'item'), SortableColumn('Color', 'color'), SortableColumn('Desc', 'desc'), SortableColumn('Misc', 'qty_misc'), SortableColumn('XS', 'qty_xs'), SortableColumn('SM', 'qty_sm'), SortableColumn('MD', 'qty_md'), SortableColumn('LG', 'qty_lg'), SortableColumn('XL', 'qty_xl'), SortableColumn('2XL', 'qty_2xl'), SortableColumn('3XL', 'qty_3xl'), SortableColumn('4XL', 'qty_4xl')])

prchGridOpen = DataGrid(fields=[('Action', lambda obj: Markup('<a href="%s">Remove</a>' % url('/delete/', params=dict(id=obj.id,ret_page='purchase',obj='PODetail',ret_id=obj.po_id)))),
                           ('Ship From', lambda obj: Markup('<input type="text" size="8" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="ship_from" value="%s"/>' % ("'ship_from'", obj.id, obj.ship_from))), 
                           ('Arrival Date', lambda obj: Markup('<input type="text" size="10"onchange="setpodetval($(this), %s, $(this).val(), %s)" name="inbnd_date" value="%s"/>' % ("'inbnd_date'", obj.id, obj.inbnd_date))),
                           ('Vendor', lambda obj: Markup('<input type="text" size="10" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="vendor_name" value="%s"/>' % ("'vendor_name'", obj.id, obj.vendor))),
                           ('Style', lambda obj: Markup('<input type="text" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="item" value="%s"/>' % ("'item'", obj.id, obj.item))),
                           ('Color', lambda obj: Markup('<input type="text" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="color" value="%s"/>' % ("'color'", obj.id, obj.color))),
                           ('Description', lambda obj: Markup('<input type="text" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="desc" value="%s"/>' % ("'desc'", obj.id, obj.desc))),
                           ('Misc', lambda obj: Markup('<input type="text" size="4" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="qty_misc" value="%s"/>' % ("'qty_misc'", obj.id, obj.qty_misc))),
                           ('XS', lambda obj: Markup('<input type="text" size="4" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="qty_xs" value="%s"/>' % ("'qty_xs'", obj.id, obj.qty_xs))),
                           ('SM', lambda obj: Markup('<input type="text" size="4" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="qty_sm" value="%s"/>' % ("'qty_sm'", obj.id, obj.qty_sm))),
                           ('MD', lambda obj: Markup('<input type="text" size="4" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="qty_md" value="%s"/>' % ("'qty_md'", obj.id, obj.qty_md))),
                           ('LG', lambda obj: Markup('<input type="text" size="4" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="qty_lg" value="%s"/>' % ("'qty_lg'", obj.id, obj.qty_lg))),
                           ('XL', lambda obj: Markup('<input type="text" size="4" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="qty_xl" value="%s"/>' % ("'qty_xl'", obj.id, obj.qty_xl))),
                           ('2XL', lambda obj: Markup('<input type="text" size="4" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="qty_2xl" value="%s"/>' % ("'qty_2xl'", obj.id, obj.qty_2xl))),
                           ('3XL', lambda obj: Markup('<input type="text" size="4" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="qty_3xl" value="%s"/>' % ("'qty_3xl'", obj.id, obj.qty_3xl))),
                           ('4XL', lambda obj: Markup('<input type="text" size="4" onchange="setpodetval($(this), %s, $(this).val(), %s)" name="qty_4xl" value="%s"/>' % ("'qty_4xl'", obj.id, obj.qty_4xl)))])

prchGridClosed = DataGrid(fields=[('Ship From', 'ship_from'), 
                           ('Arrival Date', 'inbnd_date'), 
                           ('Vendor', 'vendor'), 
                           ('Style', 'item'),
                           ('Color', 'color'),
                           ('Desc', 'desc'),
                           ('Misc', 'qty_misc'), 
                           ('XS', 'qty_xs'),
                           ('SM', 'qty_sm'),
                           ('MD', 'qty_md'),
                           ('LG', 'qty_lg'),
                           ('XL', 'qty_xl'),
                           ('2XL', 'qty_2xl'),
                           ('3XL', 'qty_3xl'),
                           ('4XL', 'qty_4xl')])

wkshtdetGrid = DataGrid(fields=[('Action', lambda obj: Markup('<a href="%s">Remove</a>' % url('/delete/', params=dict(id=obj.id,ret_page='worksheet',obj='WorksheetDetail',ret_id=obj.wk_id)))),
                           ('Customer', lambda obj: Markup('<a href="/customer/?id=%s"/>%s</a>' % (obj.orddet.order.cust_id, obj.orddet.order.Customer.name))),
                           ('Job', lambda obj: Markup('<a href="/job/?id=%s"/>%s</a>' % (obj.orddet.order.id, obj.orddet.order.job_name))),
                           ('Style', 'item.v_item'),
                           ('Color', 'item.color'),
                           ('Desc', 'item.desc'),
                           ('Misc', lambda obj: Markup('<input type="text" size="4" onchange="setwkdetval($(this), %s, $(this).val(), %s)" name="qty_misc" %s value="%s"/>' % ("'qty_misc'", obj.id, obj.icssclass, obj.qty_misc))),
                           ('XS', lambda obj: Markup('<input type="text" size="4" onchange="setwkdetval($(this), %s, $(this).val(), %s)" name="qty_xs" %s value="%s"/>' % ("'qty_xs'", obj.id, obj.icssclass, obj.qty_xs))),
                           ('SM', lambda obj: Markup('<input type="text" size="4" onchange="setwkdetval($(this), %s, $(this).val(), %s)" name="qty_sm" %s  value="%s"/>' % ("'qty_sm'", obj.id, obj.icssclass, obj.qty_sm))),
                           ('MD', lambda obj: Markup('<input type="text" size="4" onchange="setwkdetval($(this), %s, $(this).val(), %s)" name="qty_md" %s value="%s"/>' % ("'qty_md'", obj.id, obj.icssclass, obj.qty_md))),
                           ('LG', lambda obj: Markup('<input type="text" size="4" onchange="setwkdetval($(this), %s, $(this).val(), %s)" name="qty_lg" %s value="%s"/>' % ("'qty_lg'", obj.id, obj.icssclass, obj.qty_lg))),
                           ('XL', lambda obj: Markup('<input type="text" size="4" onchange="setwkdetval($(this), %s, $(this).val(), %s)" name="qty_xl" %s value="%s"/>' % ("'qty_xl'", obj.id, obj.icssclass, obj.qty_xl))),
                           ('2XL', lambda obj: Markup('<input type="text" size="4" onchange="setwkdetval($(this), %s, $(this).val(), %s)" name="qty_2xl" %s value="%s"/>' % ("'qty_2xl'", obj.id, obj.icssclass, obj.qty_2xl))),
                           ('3XL', lambda obj: Markup('<input type="text" size="4" onchange="setwkdetval($(this), %s, $(this).val(), %s)" name="qty_3xl" %s value="%s"/>' % ("'qty_3xl'", obj.id, obj.icssclass, obj.qty_3xl))),
                           ('4XL', lambda obj: Markup('<input type="text" size="4" onchange="setwkdetval($(this), %s, $(this).val(), %s)" name="qty_4xl" %s value="%s"/>' % ("'qty_4xl'", obj.id, obj.icssclass, obj.qty_4xl)))])

wkshtdetGridFrozen = DataGrid(fields=[('Customer', lambda obj: Markup('<a href="/customer/?id=%s"/>%s</a>' % (obj.orddet.order.cust_id, obj.orddet.order.Customer.name))),
                           ('Job', lambda obj: Markup('<a href="/job/?id=%s"/>%s</a>' % (obj.orddet.order.id, obj.orddet.order.job_name))),
                           ('Style', 'item.v_item'),
                           ('Color', 'item.color'),
                           ('Desc', 'item.desc'),
                           ('Misc', 'qty_misc'), 
                           ('XS', 'qty_xs'),
                           ('SM', 'qty_sm'),
                           ('MD', 'qty_md'),
                           ('LG', 'qty_lg'),
                           ('XL', 'qty_xl'),
                           ('2XL', 'qty_2xl'),
                           ('3XL', 'qty_3xl'),
                           ('4XL', 'qty_4xl')])

quoteDetail = DataGrid(fields=[('Action', lambda obj: Markup('<a href="%s">Edit</a>' % url('/orddetail/', params=dict(id=obj.PODetail.id)))),
                             ('Item #', 'item.v_item'),
                             ('Color', 'item.color'),
                             ('Desc', 'item.desc'),
                             ('XS', 'qty_xs'), 
                             ('C', 'cnt_xs'), 
                             ('P', 'prnt_xs'),
                             ('SM', 'qty_sm'), 
                             ('C', 'cnt_sm'), 
                             ('P', 'prnt_sm'),
                             ('MD', 'qty_md'), 
                             ('C', 'cnt_md'), 
                             ('P', 'prnt_md'),
                             ('LG', 'qty_lg'), 
                             ('C', 'cnt_lg'), 
                             ('P', 'prnt_lg'),
                             ('XL', 'qty_xl'), 
                             ('C', 'cnt_xl'), 
                             ('P', 'prnt_xl'),
                             ('2XL', 'qty_2xl'), 
                             ('C', 'cnt_2xl'), 
                             ('P', 'prnt_2xl'),
                             ('3XL', 'qty_3xl'), 
                             ('C', 'cnt_3xl'), 
                             ('P', 'prnt_3xl'),
                             ('4XL', 'qty_4xl'), 
                             ('C', 'cnt_4xl'), 
                             ('P', 'prnt_4xl'),
                             ('Misc', 'qty_misc'), 
                             ('C', 'cnt_misc'), 
                             ('P', 'prnt_misc'),
                             ('Units', 'units'), 
                             ('Price', 'price'), 
                             ('Total', 'total')])

printDetail = DataGrid(fields=[('Item #', 'item.v_item'),
                             ('Color', 'item.color'),
                             ('Desc', 'item.desc'),
                             ('XS', 'qty_xs'), 
                             ('SM', 'qty_sm'), 
                             ('MD', 'qty_md'), 
                             ('LG', 'qty_lg'), 
                             ('XL', 'qty_xl'), 
                             ('2XL', 'qty_2xl'), 
                             ('3XL', 'qty_3xl'), 
                             ('4XL', 'qty_4xl'), 
                             ('Misc', 'qty_misc'), 
                             ('Units', 'units'), 
                             ('Price', 'price'), 
                             ('Total', 'total'),
                             ('L1', 'l1'), 
                             ('L2', 'l2'), 
                             ('L3', 'l3'), 
                             ('L4', 'l4'), 
                             ('L5', 'l5'), 
                             ('L6', 'l6')])

printOpt = DataGrid(fields=[('Category', 'type.category.desc'), 
                             ('Option', 'type.desc'), 
                             ('Order Type', 'type.order_type.type'), 
                             ('Qty', 'qty'),
                             ('Price', 'price'), 
                             ('Total', 'total')])

printOptSqft = DataGrid(fields=[('Category', 'type.category.desc'), 
                             ('Option', 'type.desc'), 
                             ('Order Type', 'type.order_type.type'), 
                             ('Sqft', 'qty'),
                             ('Price', 'price'), 
                             ('Total', 'total')])

scrDetail = DataGrid(fields=[
	('Item', lambda obj: Markup('<input type="text" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="item" value="%s"/>' % ("'v_item'", obj.id, obj.item.v_item))), 
	('Color', lambda obj: Markup('<input type="text" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="color" value="%s"/>' % ("'color'", obj.id, obj.item.color))), 
	('Desc', lambda obj: Markup('<input type="text" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="desc" value="%s"/>' % ("'desc'", obj.id, obj.item.desc))), 
	('XS', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onload="chkzero($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="qty_xs" value="%s"/>' % ("'qty_xs'", obj.id, obj.qty_xs))), 
	('C', lambda obj: Markup('<input type="text" tabindex="-1" size="1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="cnt_xs" value="%s"/>' % ("'cnt_xs'", obj.id, "'cnt_xs'", obj.id, "'cnt_xs'", obj.id, obj.cnt_xs))),
	('P', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="prnt_xs" value="%s"/>' % ("'prnt_xs'", obj.id, "'prnt_xs'", obj.id, "'prnt_xs'", obj.id, obj.prnt_xs))),
	('SM', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onload="chkzero($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="qty_sm" value="%s"/>' % ("'qty_sm'", obj.id, obj.qty_sm))), 
	('C', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="cnt_sm" value="%s"/>' % ("'cnt_sm'", obj.id, "'cnt_sm'", obj.id, "'cnt_sm'", obj.id, obj.cnt_sm))),
	('P', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="prnt_sm" value="%s"/>' % ("'prnt_sm'", obj.id, "'prnt_sm'", obj.id, "'prnt_sm'", obj.id, obj.prnt_sm))),
	('MD', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onload="chkzero($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="qty_md" value="%s"/>' % ("'qty_md'", obj.id, obj.qty_md))), 
	('C', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="cnt_md" value="%s"/>' % ("'cnt_md'", obj.id, "'cnt_md'", obj.id, "'cnt_md'", obj.id, obj.cnt_md))),
	('P', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="prnt_md" value="%s"/>' % ("'prnt_md'", obj.id, "'prnt_md'", obj.id, "'prnt_md'", obj.id, obj.prnt_md))),
	('LG', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onload="chkzero($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="qty_lg" value="%s"/>' % ("'qty_lg'", obj.id, obj.qty_lg))), 
	('C', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="cnt_lg" value="%s"/>' % ("'cnt_lg'", obj.id, "'cnt_lg'", obj.id, "'cnt_lg'", obj.id, obj.cnt_lg))),
	('P', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="prnt_lg" value="%s"/>' % ("'prnt_lg'", obj.id, "'prnt_lg'", obj.id, "'prnt_lg'", obj.id, obj.prnt_lg))),
	('XL', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onload="chkzero($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="qty_xl" value="%s"/>' % ("'qty_xl'", obj.id, obj.qty_xl))), 
	('C', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="cnt_xl" value="%s"/>' % ("'cnt_xl'", obj.id, "'cnt_xl'", obj.id, "'cnt_xl'", obj.id, obj.cnt_xl))),
	('P', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="prnt_xl" value="%s"/>' % ("'prnt_xl'", obj.id, "'prnt_xl'", obj.id, "'prnt_xl'", obj.id, obj.prnt_xl))),
	('2XL', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onload="chkzero($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="qty_2xl" value="%s"/>' % ("'qty_2xl'", obj.id, obj.qty_2xl))), 
	('C', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="cnt_2xl" value="%s"/>' % ("'cnt_2xl'", obj.id, "'cnt_2xl'", obj.id, "'cnt_2xl'", obj.id, obj.cnt_2xl))),
	('P', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="prnt_2xl" value="%s"/>' % ("'prnt_2xl'", obj.id, "'prnt_2xl'", obj.id, "'prnt_2xl'", obj.id, obj.prnt_2xl))),
	('3XL', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onload="chkzero($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="qty_3xl" value="%s"/>' % ("'qty_3xl'", obj.id, obj.qty_3xl))), 
	('C', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="cnt_3xl" value="%s"/>' % ("'cnt_3xl'", obj.id, "'cnt_3xl'", obj.id, "'cnt_3xl'", obj.id, obj.cnt_3xl))),
	('P', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="prnt_3xl" value="%s"/>' % ("'prnt_3xl'", obj.id, "'prnt_3xl'", obj.id, "'prnt_3xl'", obj.id, obj.prnt_3xl))),
	('4XL', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onload="chkzero($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="qty_4xl" value="%s"/>' % ("'qty_4xl'", obj.id, obj.qty_4xl))), 
	('C', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="cnt_4xl" value="%s"/>' % ("'cnt_4xl'", obj.id, "'cnt_4xl'", obj.id, "'cnt_4xl'", obj.id, obj.cnt_4xl))),
	('P', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="prnt_4xl" value="%s"/>' % ("'prnt_4xl'", obj.id, "'prnt_4xl'", obj.id, "'prnt_4xl'", obj.id, obj.prnt_4xl))),
	('MISC', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onload="chkzero($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="qty_misc" value="%s"/>' % ("'qty_misc'", obj.id, obj.qty_misc))), 
	('C', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="cnt_misc" value="%s"/>' % ("'cnt_misc'", obj.id, "'cnt_misc'", obj.id, "'cnt_misc'", obj.id, obj.cnt_misc))),
	('P', lambda obj: Markup('<input type="text" size="1" tabindex="-1" ondblclick="cnt_print($(this), %s, $(this).val(), %s)" ontouchstart="cnt_print($(this), %s, $(this).val(), %s)" onchange="cnt_print($(this), %s, $(this).val(), %s)" name="prnt_misc" value="%s"/>' % ("'prnt_misc'", obj.id, "'prnt_misc'", obj.id, "'prnt_misc'", obj.id, obj.prnt_misc))),
	('Units', 'units', {'name':'units'}), 
	('Price', lambda obj: Markup('<input type="text" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="price" value="%s"/>' % ("'price'", obj.id, obj.price))), 
	('Total', 'total', {'name':'line_total'}),
	('On Order', lambda obj: Markup('<input type="checkbox" tabindex="-1" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" %s name="on_order" value="%s"/>' % ("'on_order'", obj.id, obj.checked, obj.on_order))), 
	('L1', lambda obj: Markup('<input type="text" size="2" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="l1" value="%s"/>' % ("'l1'", obj.id, obj.l1))), 
	('L2', lambda obj: Markup('<input type="text" size="2" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="l2" value="%s"/>' % ("'l2'", obj.id, obj.l2))), 
	('L3', lambda obj: Markup('<input type="text" size="2" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="l3" value="%s"/>' % ("'l3'", obj.id, obj.l3))), 
	('L4', lambda obj: Markup('<input type="text" size="2" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="l4" value="%s"/>' % ("'l4'", obj.id, obj.l4))), 
	('L5', lambda obj: Markup('<input type="text" size="2" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="l5" value="%s"/>' % ("'l5'", obj.id, obj.l5))), 
	('L6', lambda obj: Markup('<input type="text" size="2" onchange="setdetval($(this), %s, $(this).val(), %s, \'detail\')" name="l6" value="%s"/>' % ("'l6'", obj.id, obj.l6))), 
	('Action', lambda obj: Markup('<a href="%s">Remove</a>' % url('/delete/', params=dict(id=obj.id,obj="OrderDetail",ret_id=obj.ord_id,ret_page="job"))))
])
                             
optDetail = DataGrid(fields=[('Action', lambda obj: Markup('<a href="%s">Remove</a>' % url('/delete/', params=dict(id=obj.id,obj='OrderOption',ret_page='job',ret_id=obj.orddet.ord_id)))),
                             SortableColumn('Category', 'type.category.desc'), 
                             SortableColumn('Option', 'type.desc'), 
                             SortableColumn('Order Type', 'type.order_type.type'), 
                             SortableColumn('Qty', 'qty'),
                             SortableColumn('Price', 'price'), 
                             SortableColumn('Total', 'total'),
                             ('Action', lambda obj: Markup('<a href="%s">Edit</a>' % url('/orderoption/?id=' + str(obj.id))))])

optDetail2 = DataGrid(fields=[('Action', lambda obj: Markup('<a href="%s">Remove</a>' % url('/delete/', params=dict(id=obj.id,obj='OrderOption',ret_page='job',ret_id=obj.orddet.ord_id)))),
                             ('Category', 'type.category.desc'), 
                             ('Option', lambda obj: Markup('<input type="text" onchange="setdetval($(this), %s, $(this).val(), %s, \'option\')" name="desc" value="%s"/>' % ("'desc'", obj.id, obj.desc))), 
                             ('Qty', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'option\')" id="qty%s" name="qty%s" value="%s"/>' % ("'qty'", obj.id, obj.id, obj.id, obj.qty))),
                             ('Price', lambda obj: Markup('<input type="text" size="7" onfocus="keepval($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'option\')" id="price%s" name="price%s" value="%s"/>' % ("'price'", obj.id, obj.id, obj.id, obj.price))), 
                             ('Total', 'total'),
                             ])

optDetailSqft = DataGrid(fields=[('Action', lambda obj: Markup('<a href="%s">Remove</a>' % url('/delete/', params=dict(id=obj.id,obj='OrderOption',ret_page='job',ret_id=obj.orddet.ord_id)))),
                             ('Category', 'type.category.desc'), 
                             ('Option', lambda obj: Markup('<input type="text" size="128" onchange="setdetval($(this), %s, $(this).val(), %s, \'option\')" name="desc" value="%s"/>' % ("'desc'", obj.id, obj.desc))), 
                             ('Sqft', lambda obj: Markup('<input type="text" size="4" onfocus="keepval($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'option\')" name="qty" value="%s"/>' % ("'qty'", obj.id, obj.qty))),
                             ('Price', lambda obj: Markup('<input type="text" size="7" onfocus="keepval($(this))" onclick="keepval($(this))" onchange="setdetval($(this), %s, $(this).val(), %s, \'option\')" name="price" value="%s"/>' % ("'price'", obj.id, obj.price))), 
                             ('Total', 'total'),
                             ])

caddrGrid = DataGrid(fields=[('Action', lambda obj: Markup('<a href="%s">Edit</a>' % url('/address/', params=dict(id=obj.id,cust_id=obj.cust_id)))),
                             SortableColumn('Description', 'desc'), SortableColumn('Street 1', 'street1'),
                             SortableColumn('Street 2', 'street2'), SortableColumn('City', 'city'),
                             SortableColumn('State', 'state'), SortableColumn('Province', 'province'),
                             SortableColumn('Country', 'country'), SortableColumn('Zipcode', 'zipcode')])

addrGrid = DataGrid(fields=[('Action', lambda obj: Markup('<a href="%s">Edit</a>' % url('/address/', params=dict(id=obj.id,cust_id=obj.cust_id,ret_id=obj.job_id)))),
                             SortableColumn('Description', 'desc'), SortableColumn('Street 1', 'street1'),
                             SortableColumn('Street 2', 'street2'), SortableColumn('City', 'city'),
                             SortableColumn('State', 'state'), SortableColumn('Province', 'province'),
                             SortableColumn('Country', 'country'), SortableColumn('Zipcode', 'zipcode')])

cdaddrGrid = DataGrid(fields=[('Action', lambda obj: Markup('<a href="%s">Edit</a>' % url('/address/', params=dict(id=obj.id,cust_id=obj.cust_id,ret_id=0)))),
                             SortableColumn('Description', 'desc'), SortableColumn('Street 1', 'street1'),
                             SortableColumn('Street 2', 'street2'), SortableColumn('City', 'city'),
                             SortableColumn('State', 'state'), SortableColumn('Province', 'province'),
                             SortableColumn('Country', 'country'), SortableColumn('Zipcode', 'zipcode')])

mkrlogGrid = DataGrid(fields=[('Date In', 'date_in'), ('Customer', 'order.Customer.name'),
                             ('Job', 'order.job_name'), ('New', 'new_val'),
                             ('Rev', 'rev_val'), ('Due Date', 'order.proof_date'),
                             ('Rep', 'order.Rep.name')], template='nodinxprd.templates.mkrloggrid')

class OptionsForm(tw2.sqla.DbFormPage):
    entity = model.OrderOption
    resources = [tw2.core.CSSLink(link='/style.css')]
    #title = 'Options'

    class child(tw2.dynforms.CustomisedTableForm):

        class options(tw2.dynforms.GrowingGridLayout):
            category = tw2.forms.TextField()
            desc = tw2.forms.TextField()

class RootController(BaseController):
    """
    The root controller for the nodinx-prd application.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """
    secc = SecureController()
    admin = AdminController(model, DBSession, config_type=TGAdminConfig)
    vendors = DeclarativeVendorController(DBSession)
    categories = DeclarativeCategoryController(DBSession)
    orderstatus = DeclarativeOrderStatusController(DBSession)
    ordertypes = DeclarativeOrderTypeController(DBSession)
    #orderoptions = DeclarativeOrderOptionController(DBSession)
    shiptypes = DeclarativeShipTypeController(DBSession)
    proofs = DeclarativeProofController(DBSession)
    items = DeclarativeItemController(DBSession)
    itemtypes = DeclarativeItemTypeController(DBSession)
    mrkstatus = DeclarativeMarkerStatusController(DBSession)
    mrkrequestlogs = DeclarativeMarkerRequestLogController(DBSession)

    error = ErrorController()

    def _before(self, *args, **kw):
        tmpl_context.project_name = "No Dinx"

    @expose('nodinxprd.templates.index')
    @require(predicates.not_anonymous())
    def index(self):
        """Handle the front-page."""
        #return dict(page='index')
        redirect('/dashboard/?view=active')

    @expose('nodinxprd.templates.job')
    @require(predicates.not_anonymous())
    def job(self, cust_id=False, proj_id=False, new_cust=0, *args, **kw):
    	""" Handle job-data page """
        ord_total = 0.00
        units = 0
        with_options = 0
        with_details = 0
        restypes = DBSession.query(Resource).all()
        reps = DBSession.query(Rep).all()
        artists = DBSession.query(Artist).all()
        #rushes = DBSession.query(PrintTime).all()
        yes_no = DBSession.query(YesNo).all()
        ordertypes = DBSession.query(OrderType).all()
        stats = DBSession.query(OrderStatus).all()
        mstats = DBSession.query(MarkerStatus).all()
        #newrev = DBSession.query(NewRev).all()
        shiptypes = DBSession.query(ShipType).all()
        shippkup = DBSession.query(ShipPkup).all()
        recips = DBSession.query(User).filter(User.user_id > 0).order_by(User.display_name).all()
        emltmplts = DBSession.query(EmailTemplate).all()

        job_id = kw.get('id')

        if job_id:
            job_id = int(kw.get('id'))
            main_data = DBSession.query(Order).get(job_id)
            marker_data = main_data.marker
            #marker_data = DBSession.query(MarkerRequest).filter(MarkerRequest.ord_id == job_id).first()

        else:
            if cust_id:
                cust_obj = DBSession.query(Customer).get(cust_id)
            elif new_cust:
                cust_obj = Customer()
                DBSession.add(cust_obj)
                DBSession.flush()
            else:
                cust_obj = DBSession.query(Customer).get(1)

            if proj_id:
                main_data = Order(cust_id = cust_obj.id, proj_id = proj_id, ord_stat_id = 2, ord_type_id = 1, rep_id = 1, rush = 1, hold = 2, thnks_sent = 2, cust_apprv = 2, prod_apprv = 2, ship_type_id = 5, ship_pkup = 2, job_name='New Job', prod_res=1)
            else:
                main_data = Order(cust_id = cust_obj.id, ord_stat_id = 2, ord_type_id = 1, rep_id = 1, rush = 1, hold = 2, thnks_sent = 2, cust_apprv = 2, prod_apprv = 2, ship_type_id = 5, ship_pkup = 2, prod_res=1) 
             
            DBSession.add(main_data)

            proof_obj = Proof(proof_path='/img/proof_tmp.png')
            DBSession.add(proof_obj)
            DBSession.flush()
 
            marker_data = MarkerRequest(ord_id = main_data.id, status_id = 1, artist_id = 1, proof_id = proof_obj.id)
            DBSession.add(marker_data)
            DBSession.flush()
            proof_obj.marker_id = marker_data.id
            main_data.marker_id = marker_data.id
            job_id = main_data.id
            #marker_data = [marker_data]    #make it iterable

        if (main_data.OrderType.type == "Screen Print") or (main_data.OrderType.type == "Embroidery") or (main_data.OrderType.type == "Cad Cut"):
            if main_data.numsheet == '':
                main_data.numsheet = DBSession.query(Config).filter(Config.name == u'numsheet_template').first().value
            det_query = DBSession.query(OrderDetail).filter(OrderDetail.ord_id == job_id).order_by(OrderDetail.id)
            det_data = det_query.all() 
            #quote_data = det_query.all() 
            quote_total = 0.00
            ord_total = 0.00
            ord_unit_tot = 0
            units = 0
            with_details = 1
            for obj in det_data:
                if obj.on_order:
                    obj.checked = 'checked=true'
                else:
                    obj.checked = ''
                for size_qty in [obj.qty_xs, obj.qty_sm, obj.qty_md, obj.qty_lg, obj.qty_xl, obj.qty_2xl, obj.qty_3xl, obj.qty_4xl, obj.qty_misc]:
                    if size_qty is None or size_qty == '':
                        size_qty = 0
                    try:
                        units += int(str(size_qty).split('.')[0])
                    except:
                        size_qty = 0
                if obj.price is None or obj.price == '' or obj.price == 'None':
                    obj.price = 0.00
                else:
                    obj.price = Decimal(obj.price)
                if units > 0:
                    obj.total = Decimal(obj.price) * units
                    obj.units = units
                else:
                    obj.total = 0.00
                    obj.units = 0
                ord_total = Decimal(ord_total) + Decimal(obj.total)
                obj.price = '{0:.2f}'.format(Decimal(obj.price))
                obj.total = '{0:.2f}'.format(obj.total)
                ord_unit_tot += units
                units = 0
                
            #units = 0
            #for obj in quote_data:
            #    for size_qty in [obj.qty_xs, obj.qty_sm, obj.qty_md, obj.qty_lg, obj.qty_xl, obj.qty_2xl, obj.qty_3xl, obj.qty_4xl, obj.qty_misc]:
            #        if size_qty == None or size_qty == '':
            #            size_qty = 0
            #        units += int(size_qty)
            #    price = float(obj.price)
            #    if price == None or price == '':
            #        price = 0.00
            #    obj.total = price * units
            #    obj.units = units
            #    units = 0
            #    quote_total += float(obj.total)
            JobDetailGrid = scrDetail
            #quoteDetailGrid = quoteDetail
            itemChosen = {}
            avl_opts = []
            avl_catg = []
        else:
            #quoteDetailGrid = optDetail
            if main_data.ord_type_id == 4:
                JobDetailGrid = optDetailSqft
            else:
                JobDetailGrid = optDetail2
            with_options = 1
            ord_total = Decimal(0.00)
            ord_unit_tot = 0
            #quote_total = 0.00
            det_query = DBSession.query(OrderDetail).filter(OrderDetail.ord_id == job_id).order_by(OrderDetail.id)
            det_data = det_query.all() 
            #quote_data = det_query.all() 
            with_details = 0
            not_avl_opts = [0]
            avl_catg = []
            for obj in det_data:
                catg_list = []
                not_avl_opts = [0]
                if obj.price is None or obj.price == '':
                    obj.price = Decimal(0.00)

                qty = obj.qty_misc

                if qty == None or qty == '':
                    qty = 0
                obj.total = Decimal(obj.price) * Decimal(qty)
                #ord_total = Decimal(ord_total) + Decimal(obj.total)
                for opt in obj.options:
                    if opt.price == None or opt.price == '' or opt.price == 'undefined':
                        opt.price = Decimal(0.00)
                    else:
                        opt.price = Decimal(opt.price)
                    
                    if opt.qty == None or opt.qty == '' or opt.qty == 'undefined':
                        opt.qty = 0
                    #log.debug("opt.qty = " + str(opt.qty))
                    opt.total = Decimal(opt.qty) * Decimal(opt.price)
                    ord_total += Decimal(opt.total)
                    opt.total = '{0:.2f}'.format(opt.total)
                    opt.price = '{0:.2f}'.format(opt.price)
                    if opt.type.catg_id != 32:
                        catg_list.append(opt.type.catg_id)
                    else:
                        not_avl_opts.append(opt.opt_id)
                ord_total = ord_total * Decimal(qty)
                avl_opts = DBSession.query(OptionType).filter(OptionType.id == ItemOption.opt_type_id).filter(~OptionType.catg_id.in_(catg_list)).filter(~OptionType.id.in_(not_avl_opts)).filter(ItemOption.item_type_id == obj.item.type).order_by(OptionType.catg_id, OptionType.id).all()
                avl_catg = []
                for avl in avl_opts:
                    if avl.category.desc in avl_catg:
                        continue
                    else:
                        avl_catg.append(avl.category.desc)

                #for i in catg_list:
                    #log.debug("existing categories " + str(i))

                obj.avl_opts = avl_opts
                obj.avl_catg = avl_catg
                                                                                
            #for obj in quote_data:
            #    if obj.price == None or obj.price == '':
            #        obj.price = float(0.00)
            #    if obj.qty_misc == None or obj.qty_misc == '':
            #        obj.qty_misc = 0.00
            #    obj.total = float(obj.price * obj.qty_misc)
            #    quote_total += float(obj.total)
            #    for opt in obj.options:
            #        if opt.price == None or opt.price == '':
            #            opt.price = 0.00
            #        opt.total = opt.qty * float(opt.price)
            #        obj.total += float(opt.total)
            #        quote_total += float(opt.total)
            
            ph = "Select an item...."
            search = True
            c_options = DBSession.query(Item.id, Item.v_item).filter(Item.type == ItemType.id).filter(ItemType.ord_type_id == main_data.ord_type_id).order_by(Item.v_item).all()
            c_options.insert(0,(0,''))
            c_id = 'item'
            jsopts = dict(width=100)
            attrs = dict(onchange='addoitem($(this).val())')
            itemChosen = SingleSelectField(options=c_options, placeholder=ph, search_contains=search, id=c_id, value='', opts=jsopts, attrs=attrs)

        addr_data = DBSession.query(Address).filter(Address.cust_id == main_data.cust_id).order_by(Address.id).all()
        ord_total = '{0:.2f}'.format(ord_total, ',.2f')

        for adr in addr_data:
            adr.job_id = job_id

        objtype = "Order"

        emlq = DBSession.query(EmailQueue).filter(EmailQueue.ord_id == job_id).filter(EmailQueue.sent == False).order_by(EmailQueue.id).all()
        emls = DBSession.query(EmailQueue).filter(EmailQueue.ord_id == job_id).filter(EmailQueue.sent == True).order_by(EmailQueue.id).all()

        ph = "Select a customer...."
        search = True
        c_options = DBSession.query(Customer.id, Customer.name).order_by(Customer.name).all()
        c_options.insert(0,(0,''))
        c_id = 'cust_name'
        jsopts = dict(width=100)
        attrs = dict(onchange='setcust($(this).val())')
        custChosen = SingleSelectField(options=c_options, allow_single_deselect='False', placeholder=ph, search_contains=search, id=c_id, value=main_data.cust_id, opts=jsopts, attrs=attrs)

        ph = "Select a project...."
        search = True
        p_options = DBSession.query(Project.id, Project.name).order_by(Project.name).all()
        p_options.insert(0,(0,''))
        p_id = 'project'
        jsopts = dict(width=100)
        attrs = dict(onchange='setproj($(this).val())')
        projChosen = SingleSelectField(options=p_options, placeholder=ph, search_contains=search, id=p_id, value=main_data.proj_id, opts=jsopts, attrs=attrs)

        pNotes_edit = tinyEdit(id="prod_notes")
        num_edit = tinyEdit(id="numsheet")
        rNotes_edit = tinyEdit(id="rev_notes")
        sNotes_edit = tinyEdit(id="ship_notes")

        if len(marker_data.proofs):
            proof_id = marker_data.proofs[0].id
            proof_path = DBSession.query(Proof).get(proof_id).proof_path
        else:
            proof_path = '/img/proof_tmp.png'

        now = strftime('%-m/%-d/%-Y') 
        offset = timedelta(days=5)
        send_date_obj = datetime.now() + offset
        send_date = send_date_obj.strftime('%-m/%-d/%-Y')

        emllog_data = DBSession.query(EmailLog).filter(EmailLog.ord_id == job_id).all()
        for log in emllog_data:
            tmplt_obj = DBSession.query(EmailTemplate).get(log.tmplt_id)
            if tmplt_obj:
                log.type = tmplt_obj.name
            else:
                if log.tmplt_id == 9999:
                    log.type = 'Marker Request'
                elif log.tmplt_id == 9998:
                    log.type = 'Email Proof'
                elif log.tmplt_id == 9997:
                    log.type = 'Proof Ready'
                elif log.tmplt_id == 9996:
                    log.type = 'Request Invoice'
                elif log.tmplt_id == 9995:
                    log.type = 'Update Message'
                elif log.tmplt_id == 9994:
                    log.type = 'Request Job Number'
                else:
                    log.type = log.tmplt_id
            

        return dict(page='job', objtype=objtype, main_data=main_data, det_data=det_data, mkr_data=marker_data, addr_data=addr_data, job_det_form=JobDetailGrid, addrgrid=addrGrid, ord_total=ord_total, with_options=with_options, with_details=with_details, reps=reps, yes_no=yes_no, ordertypes=ordertypes, stats=stats, shiptypes=shiptypes, shippkup=shippkup, mstats=mstats, artists=artists, custchosen=custChosen, itemchosen=itemChosen, projchosen=projChosen, restypes=restypes, ord_unit_tot=ord_unit_tot, pnotes=pNotes_edit, rnotes=rNotes_edit, snotes=sNotes_edit, numsheet=num_edit, proof_path=proof_path, recips=recips, emlqs=emlq, emls=emls, now=now, send_date=send_date, emltmplts=emltmplts, emllog_data=emllog_data, emlloggrid=emllogGrid)

    #--->

    @expose('nodinxprd.templates.print-job-sheet')
    @require(predicates.not_anonymous())
    def printjobsheet(self, job_id, *args, **kw):
    	""" Handle job-sheet printing page """

        import re

        shippkup = DBSession.query(ShipPkup).all()
        ord_total = 0.00
        ord_unit_tot = 0
        pattern = re.compile('ok')

        if job_id:
            job_id = int(job_id)
            main_data = DBSession.query(Order).get(job_id)
        else:
            redirect('/job/?id=' + str(job_id))

        shiptype = DBSession.query(ShipType).get(main_data.ship_type_id).type
        if not shiptype:
            shiptype = 'None'

        if main_data.ship_pkup == 1:
            ship_pkup = 'Ship'
        else:
            ship_pkup = 'Pickup'

        if main_data.prod_apprv == 1:
            prod_apprv = 'Yes'
        else:
            prod_apprv = 'No'

        addr_data = DBSession.query(Address).filter(Address.id == main_data.cust_id).first()
        tot_units = 0

        if (main_data.OrderType.type == u'Screen Print') or (main_data.OrderType.type == u'Embroidery') or (main_data.OrderType.type == u'Cad Cut'):
            det_query = DBSession.query(OrderDetail).filter(OrderDetail.ord_id == job_id).order_by(OrderDetail.id)
            det_data = det_query.all() 
            units = 0
            with_details = 1
            with_options = 0
            for obj in det_data:
                for size_qty in [obj.qty_xs, obj.qty_sm, obj.qty_md, obj.qty_lg, obj.qty_xl, obj.qty_2xl, obj.qty_3xl, obj.qty_4xl, obj.qty_misc]:
                    if size_qty == None or size_qty == '':
                        size_qty = 0
                    units += int(str(size_qty).split('.')[0])
                if obj.price == None or obj.price == '':
                    obj.price = 0.00
                else:
                    obj.price = obj.price
                if units > 0:
                    obj.total = Decimal(obj.price) * units
                    obj.units = units
                else:
                    obj.total = 0.00
                    obj.units = 0
                ord_total = Decimal(ord_total) + Decimal(obj.total)
                obj.price = '{0:.2f}'.format(Decimal(obj.price))
                obj.total = '{0:.2f}'.format(obj.total)
                ord_unit_tot += units
                units = 0
                
            detGrid = printDetail

        else:
            if (main_data.ord_type_id == 4):
                detGrid = printOptSqft
            else:
                detGrid = printOpt
            with_options = 1
            det_query = DBSession.query(OrderDetail).filter(OrderDetail.ord_id == job_id)
            mstr_det = det_query.all() 
            if len(mstr_det):
                det_data = DBSession.query(OrderOption).filter(OrderOption.ordd_id == mstr_det[0].id).all()
            else:
                det_data = []
            with_details = 0

            for obj in mstr_det:
                if obj.price == None or obj.price == '':
                    obj.price = Decimal(0.00)

                qty = obj.qty_misc

                if qty == None or qty == '':
                    qty = 0
                obj.total = Decimal(obj.price) * Decimal(qty)
                ord_total = Decimal(ord_total) + Decimal(obj.total)
                for opt in obj.options:
                    if opt.price == None or opt.price == '':
                        opt.price = Decimal(0.00)
                    else:
                        opt.price = Decimal(opt.price)
                    opt.total = Decimal(opt.qty) * Decimal(opt.price)
                    ord_total += Decimal(opt.total)
                    opt.total = '{0:.2f}'.format(opt.total)
                    opt.price = '{0:.2f}'.format(opt.price)

        main_data.print_notes = Markup(main_data.prod_notes)
        ord_total = '{0:.2f}'.format(ord_total, ',.2f')
        tot_units = ord_unit_tot

        return dict(page='printjobsheet', main_data=main_data, det_data=det_data, detgrid=detGrid, with_details=with_details, with_options=with_options, ord_total=ord_total, ord_unit_tot=ord_unit_tot, prod_apprv=prod_apprv, ship_pkup=ship_pkup, shiptype=shiptype, addr_data=addr_data, tot_units=tot_units)

    #--->

    @expose('nodinxprd.templates.dashboard')
    @require(predicates.not_anonymous())
    #@paginate("data", items_per_page=25)
    def dashboard(self, filter=False, view="active", *args, **kw):
        """Handle the 'dashboard' page."""

        t_map = {"Customer":"customers", "OrderType":"ordertypes", "Order":"orders", "OrderStatus":"orderstats", "MarkerStatus":"markerstatuses"}
        f_map = {"customer":"customers.name", "job_name":"orders.job_name", "job_num":"orders.job_num", "ord_type":"ordertypes.type", "order_date":"orders.order_date", "process_date":"orders.proc_date", "due_date":"orders.due_date", "accnt_rep":"orders.Rep.name", "ord_stat":"orderstats.status", "MarkerStatus":"markerstatuses", "last_edit":"orders.change_dtime"}

        if kw.get('search_field') == None:
            filter = False

        if view == 'archive':
            archive = True
            all = False
            active = False
            filter = False
            pending = False
            myjobs = False
            view = 'Archive'
        elif view == 'all':
            all = True
            archive = False
            active = False
            filter = False
            pending = False
            myjobs = False
            view = 'All'
        elif view == 'pending':
            pending = True
            active = False
            archive = False
            all = False
            filter = False
            myjobs = False
            view = 'Pending'
        elif view == 'myjobs':
            myjobs = True
            active = False
            archive = False
            all = False
            filter = False
            pending = False
            the_user = request.identity['repoze.who.userid']
            user_id = DBSession.query(User).filter(User.user_name == the_user).one().user_id
            res = DBSession.query(Rep.id).filter(Rep.user_id == user_id).all()
            rep_ids = []
            for tup in res:
                rep_ids.append(tup[0])
            view = the_user.capitalize() + "\'s"
            #log.debug("rep_ids = "  + str(rep_ids))
        elif view == 'active':
            active = True
            archive = False
            all = False
            filter = False
            pending = False
            myjobs = False
            view = 'Active'

        ordering = kw.get('ordercol')

        if filter:
            #log.debug(kw.get('search_field'))
            vfield = f_map[kw.get('search_field')]
            pattern = kw.get('search_pattern')

        if not ordering:
            if filter:
                data = DBSession.query(Order, Customer, OrderStatus, OrderType).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).filter(Order.archived == False).filter(vfield + " LIKE \"%" + pattern + "%\"").order_by(Customer.name).all()
            elif active:
                data = DBSession.query(Order, Customer, OrderStatus, OrderType).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).filter(OrderStatus.status != u'Pending').filter(Order.archived == False).order_by(Customer.name).all()
            elif myjobs:
                data = DBSession.query(Order, Customer, OrderStatus, OrderType).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).filter(Order.archived == False).filter(Order.rep_id.in_(rep_ids)).order_by(Customer.name).all()
            elif all:
                data = DBSession.query(Order, Customer, OrderStatus, OrderType).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).order_by(Customer.name).all()
            elif archive:
                data = DBSession.query(Order, Customer, OrderStatus, OrderType).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).filter(Order.archived == True).order_by(Customer.name).all()
            elif pending:
                data = DBSession.query(Order, Customer, OrderStatus, OrderType).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(OrderStatus.status == u'Pending').filter(Order.ord_type_id == OrderType.id).order_by(Customer.name).all()
            else:
                data = DBSession.query(Order, Customer, OrderStatus, OrderType).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).filter(Order.archived == False).order_by(Customer.name).all()
        #else:
            #t_name, c_name = ordering[1:].split('.')
            #t_c = t_map[t_name] + '.' + c_name

            #if ordering and ordering[0] == '+':
                #if filter:
                    #data = DBSession.query(Order, Customer, OrderStatus, OrderType, MarkerStatus).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).filter(Order.archived == False).outerjoin(MarkerRequest).filter(MarkerRequest.status_id == MarkerStatus.id).filter(vfield + " LIKE \"%" + pattern + "%\"").order_by(asc(t_c)).all()
                #else:
                    #data = DBSession.query(Order, Customer, OrderStatus, OrderType, MarkerStatus).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).filter(Order.archived == False).outerjoin(MarkerRequest).filter(MarkerRequest.status_id == MarkerStatus.id).order_by(asc(t_c)).all()
            #elif ordering and ordering[0] == '-':
                #if filter:
                    #data = DBSession.query(Order, Customer, OrderStatus, OrderType, MarkerStatus).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).filter(Order.archived == False).outerjoin(MarkerRequest).filter(MarkerRequest.status_id == MarkerStatus.id).filter(vfield + " LIKE \"%" + pattern + "%\"").order_by(desc(t_c)).all()
                #else:
                    #data = DBSession.query(Order, Customer, OrderStatus, OrderType, MarkerStatus).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).filter(Order.archived == False).outerjoin(MarkerRequest).filter(MarkerRequest.status_id == MarkerStatus.id).order_by(desc(t_c)).all()

        return dict(page='dashboard', jobgrid=jobGrid, data=data, view=view)

    #--->

    @expose('nodinxprd.templates.customer')
    @require(predicates.not_anonymous())
    def customer(self, *args, **kw):
    	""" Handle customer-data page """
        id = kw.get('id')

        if id:
            id = int(kw.get('id'))
            cust_data = DBSession.query(Customer).get(id)
        else:
            cust_data = Customer()
            DBSession.add(cust_data)
            DBSession.flush()
            id = cust_data.id


        addr_data = DBSession.query(Address).filter(Address.cust_id == id).order_by(Address.id).all()

        addr_cnt = len(addr_data)

        job_data = DBSession.query(Order, Customer, OrderStatus, OrderType, MarkerStatus).filter(Order.cust_id == id).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).outerjoin(MarkerRequest).filter(MarkerRequest.status_id == MarkerStatus.id).order_by(Order.job_name)
                
        objtype = "Customer"

        return dict(page='customer', objtype=objtype, cust_data=cust_data, cdaddrgrid=cdaddrGrid, addr_data=addr_data, job_data=job_data, jobgrid=cjobGrid)

    #--->

    @expose('nodinxprd.templates.customers')
    @require(predicates.not_anonymous())
    def customers(self, filter=False, *args, **kw):
        """Handle the 'customers' page."""

        f_map = {"name":"customers.name", "ae_num":"customers.ae_num", "cur_job_num":"customers.cur_job_num", "contact":"customers.contact", "email":"customers.email", "phone":"customwers.phone", "notes":"customers.notes"}

        if kw.get('search_field') == None:
            filter = False

        if filter:
            #log.debug(kw.get('search_field'))
            vfield = f_map[kw.get('search_field')]
            pattern = kw.get('search_pattern')

        ordering = kw.get('ordercol')

        if not ordering:
            if filter:
                data = DBSession.query(Customer).filter(vfield + " LIKE \"%" + pattern + "%\"").order_by(Customer.name)
            else:
                data = DBSession.query(Customer).order_by(Customer.name)
        else:
            if ordering and ordering[0] == '+':
                if filter:
                    data = DBSession.query(Customer).filter(vfield + " LIKE \"%" + pattern + "%\"").order_by(asc(ordering[1:]))
                else:
                    data = DBSession.query(Customer).order_by(asc(ordering[1:]))
            elif ordering and ordering[0] == '-':
                if filter:
                    data = DBSession.query(Customer).filter(vfield + " LIKE \"%" + pattern + "%\"").order_by(desc(ordering[1:]))
                else:
                    data = DBSession.query(Customer).order_by(desc(ordering[1:]))

        return dict(page='customers', custgrid=custGrid, data=data)

    #--->

    @expose('nodinxprd.templates.project')
    @require(predicates.not_anonymous())
    def project(self, *args, **kw):
    	""" Handle job-data page """
        reps = DBSession.query(Rep).all()
        stats = DBSession.query(ProjectStat).all()
        proj_id = kw.get('id')
        pNotes_edit = tinyEdit(id="notes")

        if proj_id:
            proj_id = int(kw.get('id'))
            proj_data = DBSession.query(Project).get(proj_id)
        else:
            proj_data = Project(status_id = 1, rep_id = 1)
            DBSession.add(proj_data)
            DBSession.flush()
            proj_id = proj_data.id

        
        job_data = DBSession.query(Order, Customer, OrderStatus, OrderType, MarkerStatus).filter(Order.proj_id == proj_id).filter(Order.cust_id == Customer.id).filter(Order.ord_stat_id == OrderStatus.id).filter(Order.ord_type_id == OrderType.id).outerjoin(MarkerRequest).filter(MarkerRequest.status_id == MarkerStatus.id).order_by(Order.job_name)
                
        objtype = "Project"

        return dict(page='project', proj_data=proj_data, job_data=job_data, reps=reps, stats=stats, jobgrid=jobGrid, objtype=objtype, pnotes=pNotes_edit)

    #--->

    @expose('nodinxprd.templates.projects')
    @require(predicates.not_anonymous())
    @paginate("data", items_per_page=25)
    def projects(self, filter=False, *args, **kw):
        """Handle the 'projects' page."""

        f_map = {"name":"projects.name", "status":"projects.prj_stat.proj_stat", "due_date":"projects.proj_due", "rep":"projects.reps.name"}

        if kw.get('search_field') == None:
            filter = False

        if filter:
            #log.debug(kw.get('search_field'))
            vfield = f_map[kw.get('search_field')]
            pattern = kw.get('search_pattern')

        ordering = kw.get('ordercol')

        if not ordering:
            if filter:
                data = DBSession.query(Project).filter(vfield + " LIKE \"%" + pattern + "%\"").order_by(Project.name).all()
            else:
                data = DBSession.query(Project).order_by(Project.name).all()
        else:
            if ordering and ordering[0] == '+':
                if filter:
                    data = DBSession.query(Project).filter(vfield + " LIKE \"%" + pattern + "%\"").order_by(asc(ordering[1:])).all()
                else:
                    data = DBSession.query(Project).order_by(asc(ordering[1:])).all()
            elif ordering and ordering[0] == '-':
                if filter:
                    data = DBSession.query(Project).filter(vfield + " LIKE \"%" + pattern + "%\"").order_by(desc(ordering[1:])).all()
                else:
                    data = DBSession.query(Project).order_by(desc(ordering[1:]))

        if not data:
            data = []

        return dict(page='projects', projgrid=projGrid, data=data)

    #--->

    @expose('nodinxprd.templates.loadcsv')
    @require(predicates.not_anonymous())
    def loadcsv(self, *args, **kw):
    	""" Handle loadcsv-data page """

        ordertypes = DBSession.query(OrderType).all()
        ph = "Select a customer...."
        search = True
        c_options = DBSession.query(Customer.id, Customer.name).order_by(Customer.name).all()
        c_options.insert(0,(0,''))
        c_id = 'cust_name'
        jsopts = dict(width=100)
        attrs = dict()
        custChosen = SingleSelectField(options=c_options, allow_single_deselect='False', placeholder=ph, search_contains=search, id=c_id, opts=jsopts, attrs=attrs)

        return dict(page='loadcsv', custchosen=custChosen, ordertypes=ordertypes)

    #--->

    @expose('nodinxprd.templates.processcsv')
    @require(predicates.not_anonymous())
    def processcsv(self, csvfile, cust_name, ord_type, *args, **kw):
    	""" Handle processcsv page """

        import sys, csv, re
        lines = 0
        data = []
        tb_map = {'x-small':3, 'small':6, 'medium':9, 'large':12, 'x-large':15, '2x-large':18}
        pat1 = re.compile('youth')
        try:
            csv_reader = csv.reader(csvfile.file)
            rep_id = DBSession.query(Rep.id).filter(Rep.user_id == request.identity['repoze.who.userid']).first()

            if not rep_id:
                rep_id = 1

            cust_obj = DBSession.query(Customer).get(cust_name)
            job_name = 'Spirit Store ' + cust_obj.name

            ord_obj = Order(cust_id=cust_name, job_name=job_name, ord_type_id=ord_type, ord_stat_id=2, rep_id=rep_id, rush=1, hold=2, thnks_sent=2, cust_apprv=2, prod_apprv=2, ship_type_id=5, ship_pkup=2, prod_res=1)
            DBSession.add(ord_obj)
            DBSession.flush()

            proof_obj = Proof(proof_path='/img/proof_tmp.png')
            DBSession.add(proof_obj)
            DBSession.flush()

            marker_data = MarkerRequest(ord_id=ord_obj.id, status_id=1, artist_id=1, proof_id=proof_obj.id)
            DBSession.add(marker_data)
            DBSession.flush()

            ord_obj.marker_id = marker_data.id
            proof_obj.marker_id = marker_data.id
            for i, row in enumerate(csv_reader):
                if len(row) == 0 or row[0] == '' or i == 1:
                    if len(row) > 21 and row[0] == '':
                        col_map = {}
                        for j, col in enumerate(row):
                            if j < 21 or col == 'C' or col == 'P':
                                continue
                            else:
                                res = col.split('-')

                                if pat1.match(col):
                                    if len(res) > 2:
                                        size = res[1] + '-' + res[2]
                                    else:
                                        size = res[1]
                                else:
                                    size = col

                                if size == '':
                                    continue

                                col_map[size] = j

                                if col == '3x-large':
                                    tb_map[col] = j
                                elif col == '4x-large':
                                    tb_map[col] = j
                                elif col == 'one-size':
                                    tb_map[col] = j
                    continue 
                if i == 0:
                    heading = row[0] + ' - ' + row[1]
                    continue

                a_row = row
                data.append(a_row)

                item_obj = Item(type=22, v_item=row[0].decode('utf-8'), color=row[1].decode('utf-8'), desc=row[2].decode('utf-8'))
                DBSession.add(item_obj)
                DBSession.flush()

                item_pat = re.compile("NO SKU")
                if item_pat.match(row[0]):
                    qty_misc = row[21]
                else:
                    try:
                        qty_misc = row[int(col_map['one-size'])]
                    except:
                        qty_misc = ''

                for key in col_map:
                    if key == '3x-large' or key == '4x-large' or key == 'one-size':
                        continue
                    row[int(tb_map[key])] = int(row[int(tb_map[key])]) + int(row[int(col_map[key])])

                try:
                    qty_3xl = row[col_map['3x-large']]
                except:
                	qty_3xl = ''

                try:
                    qty_4xl = row[col_map['4x-large']]
                except:
                	qty_4xl = ''

                for k, qty in enumerate(row):
                    if qty == '0':
                        row[k] = ''

                orddet = OrderDetail(ord_id=ord_obj.id, item_id=item_obj.id, qty_xs=row[3], qty_sm=row[6], qty_md=row[9], qty_lg=row[12], qty_xl=row[15], qty_2xl=row[18], qty_3xl=qty_3xl, qty_4xl=qty_4xl, qty_misc=qty_misc)
                DBSession.add(orddet)
                DBSession.flush()
                lines += 1
        #except Exception as inst:
        except NameError:
            #log.debug(inst.args[0])
            flash("Unable to load the file, check that the data format is csv compliant", 'danger')
            redirect('/loadcsv')
        DBSession.flush()

        return dict(page='processcsv', heading=heading, data=data, lines=lines, job_id=ord_obj.id, job_name=ord_obj.job_name)

    #--->

    @expose('nodinxprd.templates.inbound')
    @require(predicates.not_anonymous())
    @paginate("data", items_per_page=25)
    def inbound(self, *args, **kw):
        """Handle the inbound page."""

        inbnd_date = kw.get('inbnd_date')

        if not inbnd_date:
            inbnd_date = strftime('%-m/%-d') 
            data = DBSession.query(PODetail).filter(PODetail.inbnd_date == inbnd_date).order_by(PODetail.id)
        else:
            data = DBSession.query(PODetail).filter(PODetail.inbnd_date == inbnd_date).order_by(PODetail.id)

        return dict(page='inbound', inbdgrid=inbdGrid, data=data, inbnd_date=inbnd_date)

    #--->

    @expose('nodinxprd.templates.purchase')
    @require(predicates.not_anonymous())
    #@paginate("pod_data", items_per_page=25)
    def purchase(self, *args, **kw):
        """Handle the 'purchase' page."""

        po_id = kw.get('id')

        if not po_id:
            po_id = DBSession.query(func.max(PO.id)).scalar()

        po_data = DBSession.query(PO).get(po_id)

        pod_data = DBSession.query(PODetail).filter(PODetail.po_id == po_id).order_by(PODetail.id).all()

        if po_data.closed:
            cssclass = {'class': 'panel panel-info'}
            for detail in pod_data:
                detail.icssclass = 'disabled'
            hcssclass = {'class': 'panel-title panel-info'}
            phrase = 'Closed PO'
            prchgrid = prchGridClosed
        else:
            cssclass = {'class': 'panel panel-default'}
            for detail in pod_data:
                detail.icssclass = ''
            hcssclass = {'class': 'panel-title'}
            phrase = 'Open PO'
            prchgrid = prchGridOpen

        objtype = "PO"

        return dict(page='purchase', prchgrid=prchgrid, pod_data=pod_data, po_data=po_data, objtype=objtype, cssclass=cssclass, hcssclass=hcssclass, phrase=phrase)

    #--->

    @expose('nodinxprd.templates.po_s')
    @require(predicates.not_anonymous())
    #@paginate("po_data", items_per_page=25)
    def po_s(self, *args, **kw):
        """Handle the 'po_s' page."""

        ordering = kw.get('ordercol')

        if not ordering:
            data = DBSession.query(PO).order_by(PO.id.desc())
        else:
            if ordering and ordering[0] == '+':
                data = DBSession.query(PO).order_by(asc(ordering[1:]))
            elif ordering and ordering[0] == '-':
                data = DBSession.query(PO).order_by(desc(ordering[1:]))

        return dict(page='po_s', pogrid=poGrid, po_data=data)

    #--->

    @expose('nodinxprd.templates.podetail')
    @require(predicates.not_anonymous())
    #@paginate("pod_data", items_per_page=125)
    def podetail(self, *args, **kw):
        """Handle the 'podetail' page."""

        vendors = DBSession.query(Vendor).order_by(Vendor.id).all()

        ordering = kw.get('ordercol')
        pod_id = kw.get('id')

        if pod_id:
            pod_data = DBSession.query(PODetail).get(pod_id)
        else:
            pod_id = DBSession.query(func.max(PODetail.id)).scalar()

        ph = "Select a vendor...."
        search = True
        c_options = DBSession.query(Vendor.id, Vendor.name).order_by(Vendor.name).all()
        c_options.insert(0,(0,''))
        c_id = 'vendor'
        jsopts = dict(width=100)
        attrs = dict(onchange='setvendor($(this).val())')
        vendorChosen = SingleSelectField(options=c_options, placeholder=ph, search_contains=search, id=c_id, value=pod_data.vend_id,opts=jsopts, attrs=attrs)

        ph = "Select an item...."
        search = True
        c_options = DBSession.query(Item.id, Item.v_item).order_by(Item.v_item).all()
        c_options.insert(0,(0,''))
        c_id = 'item'
        jsopts = dict(width=100)
        attrs = dict(onchange='setitem($(this).val())')
        itemChosen = SingleSelectField(options=c_options, placeholder=ph, search_contains=search, id=c_id, value=pod_data.item_id, opts=jsopts, attrs=attrs)

        objtype = "PODetail"

        return dict(page='podetail', pod_data=pod_data, objtype=objtype, vendorchosen=vendorChosen, itemchosen=itemChosen)

    #--->

    @expose('nodinxprd.templates.worksheet')
    @require(predicates.not_anonymous())
    def worksheet(self, *args, **kw):
        """Handle the 'worksheet' page."""

        wk_id = kw.get('id')
        wk_data = DBSession.query(Worksheet).get(wk_id)
        det_data = DBSession.query(WorksheetDetail).filter(WorksheetDetail.wk_id == wk_id).order_by(WorksheetDetail.id).all()

        if wk_data.frozen:
            cssclass = {'class': 'panel panel-info'}
            for detail in det_data:
                detail.icssclass = 'disabled'
            hcssclass = {'class': 'panel-title panel-info'}
            phrase = 'Frozen Worksheet'
            wkshtgrid = wkshtdetGridFrozen
        else:
            cssclass = {'class': 'panel panel-default'}
            for detail in det_data:
                detail.icssclass = ''
            hcssclass = {'class': 'panel-title'}
            phrase = 'Current Worksheet'
            wkshtgrid = wkshtdetGrid

        objtype = "Worksheet"

        return dict(page='worksheet', wkshtgrid=wkshtgrid, wk_data=wk_data, det_data=det_data, objtype=objtype, cssclass=cssclass, hcssclass=hcssclass, phrase=phrase)

    #--->

    @expose('nodinxprd.templates.worksheets')
    @require(predicates.not_anonymous())
    @paginate("wkshts_data", items_per_page=25)
    def worksheets(self, *args, **kw):
        """Handle the 'worksheets' page."""

        data = DBSession.query(Worksheet).order_by(Worksheet.id.desc()).all()
        for obj in data:
            obj.num = DBSession.query(PO.num).filter(PO.id == obj.po_id).first()

        return dict(page='worksheets', wkshtsgrid=wkshtsGrid, wkshts_data=data)

    #--->

    @expose('nodinxprd.templates.orddetail')
    @require(predicates.not_anonymous())
    def orddetail(self, ord_id=0, *args, **kw):
        """Handle the orddetail page."""

        id = kw.get('id')

        if not id:
            new_item = Item(v_item='', color='', desc='')
            DBSession.add(new_item)
            DBSession.flush()
            det_data = OrderDetail(ord_id=ord_id, item_id=new_item.id)
            DBSession.add(det_data)
            DBSession.flush()
        else:
            det_data = DBSession.query(OrderDetail).get(id)

        ph = "Select an item...."
        search = True
        c_options = DBSession.query(Item.id, Item.v_item).order_by(Item.v_item).all()
        c_options.insert(0,(0,''))
        c_id = 'item'
        jsopts = dict(width=100)
        attrs = dict(onchange='setitem($(this).val())')
        itemChosen = SingleSelectField(options=c_options, placeholder=ph, search_contains=search, id=c_id, value=det_data.id,opts=jsopts, attrs=attrs)
        objtype = "OrderDetail"

        return dict(page='orddetail', det_data=det_data, objtype=objtype, itemchosen=itemChosen)

    #--->

    @expose('nodinxprd.templates.orderoption')
    @require(predicates.not_anonymous())
    def orderoption(self, id, *args, **kw):
        """Handle the option page."""

        if not id:
            flash("No option found!", "warning")
        else:
            opt_data = DBSession.query(OrderOption).get(id)
            if opt_data.active:
                checkAct = {'checked':'True'}
            else:
                checkAct = {'checked':None}

            if opt_data.pushed:
                checkPush = {'checked':'True'}
            else:
                checkPush = {'checked':None}

        objtype = "OrderOption"

        return dict(page='orderoption', opt_data=opt_data, objtype=objtype, checkact=checkAct, checkpush=checkPush)

    #--->

    @expose('nodinxprd.templates.address')
    @require(predicates.not_anonymous())
    def address(self, id=0, ret_id=0, *args, **kw):
        """Handle the address page."""

        if id:
            addr_obj = DBSession.query(Address).get(id)
        else:
            addr_obj = Address(cust_id=kw.get('cust_id'))
            DBSession.add(addr_obj)
            DBSession.flush()

        if kw.get('cust_id'):
            cust_id = kw.get('cust_id')
        else:
            cust_id = addr_obj.cust_id 
        cust_name = DBSession.query(Customer.name).filter(Customer.id == cust_id).first()[0]

        objtype = "Address"

        return dict(page='address', addr_data=addr_obj, objtype=objtype, cust_name=cust_name, job_id=ret_id)

    #--->

    @expose('nodinxprd.templates.mrkrequestlog')
    @require(predicates.not_anonymous())
    def markerlog(self, *args, **kw):
        """Handle the marker request log page."""

        mkrlog_obj = DBSession.query(MarkerRequestLog).order_by(MarkerRequestLog.id.desc()).all()

        for mkrlog in mkrlog_obj:
            if mkrlog.MarkerRequest.status_id == 1:
                mkrlog.new_val = 'X'
                mkrlog.rev_val = ''
            else:
                mkrlog.new_val = ''
                mkrlog.rev_val = 'X'

        mkrloggrid = mkrlogGrid
        objtype = "MarkerRequestLog"

        return dict(page='markerlog', mkrlog_data=mkrlog_obj, mkrloggrid=mkrloggrid, objtype=objtype)

    #--->

    @expose('nodinxprd.templates.config')
    @require(predicates.not_anonymous())
    def config(self, *args, **kw):
        """Handle the config page."""

        cfg_data = DBSession.query(Config).first()

        objtype = "Config"

        return dict(page='config', cfg_data=cfg_data, objtype=objtype)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def addmarker(self, ord_id=0, *args, **kw):
        """Handle the addmarker page."""

        count = DBSession.query(MarkerRequest).filter(MarkerRequest.ord_id == ord_id).count() + 1
        proof_obj = Proof()
        DBSession.add(proof_obj)
        DBSession.flush()
        mkr_obj = MarkerRequest(ord_id=ord_id, cnt = count, proof_id=proof_obj.id, rev_num=1, status_id=1, artist_id=1)
        DBSession.add(mkr_obj)
        DBSession.flush()
        #mkr_data = DBSession.query(MarkerRequest).filter(ord_id == ord_id).all()

        objtype = "Order"

        redirect("/job" + "?id=" + ord_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def loadproof(self, proof, id, ord_id, job_num, *args, **kw):
        """Handle the uploadproof page."""
        
        #from PIL import Image
        ae_num = str(job_num).split('.')[0]
            
        if not ae_num or not job_num:
            flash("You must assign an Account Edge number and job number first!", "danger")
            redirect("/job/" + "?id=" + ord_id)

        jobarray = str(job_num).split('.')[1:]
        if len(jobarray) > 2:
            job = '.'.join(jobarray)
        elif len(jobarray) == 2:
            job = '.'.join(jobarray)
        elif len(jobarray) == 1:
            job = jobarray[0]
        else:
            flash("You must assign an Account Edge number and job number first!", "danger")
            redirect("/job/" + "?id=" + ord_id)

        #log.debug("proof name: " + proof.filename + '   job :' + job)
        fullpath = 'nodinxprd/public/res/Customer_jobs/' + ae_num + '/' + job + '/revisions/' + proof.filename 
        proof_path = '/res/Customer_jobs/' + ae_num + '/' + job + '/revisions/' + proof.filename
        if not os.path.exists('nodinxprd/public/res/Customer_jobs/' + ae_num + '/' + job + '/revisions'):
            os.makedirs('nodinxprd/public/res/Customer_jobs/' + ae_num + '/' + job + '/revisions', 0777)
        output = open(str(fullpath).encode('utf-8'), 'w+')
        #img = Image.open(proof.file)
        #img.resize((618,800))
        #img.save(output, format='PNG')
        bytes = proof.file.read()
        output.write(bytes)
        output.flush()
        output.close()

        proof_obj = Proof(marker_id=id, proof_path=proof_path)
        DBSession.add(proof_obj)
        #proof_obj.proof_path = proof_path
        DBSession.flush()

        objtype = "Order"

        redirect("/job/" + "?id=" + ord_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setproofpath(self, proof_path, id, ord_id, *args, **kw):
        """Handle the setproofpath page."""

        proof_obj = DBSession.query(Proof).get(id) 
        proof_obj.proof_path = proof_path
        DBSession.flush()

        objtype = "Order"

        redirect("/job/" + "?id=" + ord_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setcooloff(self, **kw):
        try:
           Object = DBSession.query(Config).filter(Config.name == 'cool-off-delay').first()
           Object.value = kw.get('delay')
           DBSession.flush()

        except():
            flash("Error saving edit!", "danger")
            redirect("/emailmkt/")

        flash("Success updating email template!", "success")
        redirect("/emailmkt/")

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def settmpltactive(self, id, **kw):
        try:
            if id:
               Object = DBSession.query(EmailTemplate).get(id)
               if Object.active:
                   Object.active = 0
               else:
                   Object.active = 1
               DBSession.flush()

        except():
            flash("Error saving edit!", "danger")
            redirect("/dashboard/")

        #flash("Success updating email template!", "success")

    #--->

    @expose('nodinxprd.templates.emailmkt')
    @require(predicates.not_anonymous())
    def emailmkt(self, *args, **kw):
    	""" Handle job-data page """

        tmplt_data = DBSession.query(EmailTemplate).all()

        for obj in tmplt_data:
            if obj.active:
                obj.checked = 'checked=true'
            else:
                obj.checked = ''
                
        cfg = DBSession.query(Config).filter(Config.name == u'cool-off-delay').first()
        delay = cfg.value

        return dict(page='emlmkt', tmplt_data=tmplt_data, emltmpltgrid=emlTmpltGrid, delay=delay)

    #--->

    @expose()
    def sendmktemail(self, *args, **kw):
        """Send marketing emails"""

        import re

        cfg = DBSession.query(Config).filter(Config.name == u'cool-off-delay').first()
        emlq = DBSession.query(EmailQueue).filter(EmailQueue.sent == False).filter(EmailQueue.status_id == 1).all()
        for eml in emlq:
            send_date = parse(eml.send_date)
            today = date.today()
            #log.debug("today = " + today.strftime('%-m/%-d/%-y'))

            if send_date.strftime('%-m/%-d/%-y') != today.strftime('%-m/%-d/%-y'):
                continue

            #log.debug("entered sendmktemail function send_date = " + send_date.strftime('%-m/%-d/%-y'))

            tmplt_obj = DBSession.query(EmailTemplate).filter(EmailTemplate.id == eml.tmplt_id).first()
            ord_obj = DBSession.query(Order).get(eml.ord_id)

            if ord_obj.Customer.last_mkt_msg:
                last = parse(ord_obj.Customer.last_mkt_msg)
                if (date.today() - last.date()).days < int(cfg.value):
                    continue

            marker = DBSession.query(MarkerRequest).get(ord_obj.marker_id)
            sender = 'info@nodinx.com'
            recipient = ord_obj.Customer.email

            if len(eml.subject) == 0:
                subject = tmplt_obj.subject
            else:
                subject = eml.subject

            if len(eml.mesg) == 0:
                message = tmplt_obj.message
            else:
                message = eml.mesg

            ret_id = ord_obj.id
            cust_name = ord_obj.Customer.name
            contact = ord_obj.Customer.contact
            job_name = ord_obj.job_name
            rep_name = ord_obj.Rep.name
            job_num = ord_obj.job_num
            artist = marker.artist.name
            artist_email = marker.artist.user.email_address
            img = tmplt_obj.img
            content = tmplt_obj.value

            cust_name_pat = re.compile("customer\.name")
            content = cust_name_pat.sub(cust_name, content)
            cust_email = re.compile("customer\.email")
            content = cust_email.sub(recipient, content)
            cust_contact_pat = re.compile("customer\.contact") 
            content = cust_contact_pat.sub(contact, content)
            job_name_pat = re.compile("job\.name")
            content = job_name_pat.sub(job_name, content)
            job_id_pat = re.compile("job\.id")
            content = job_id_pat.sub(str(ret_id), content)
            img_pat = re.compile("\/res/Customer_jobs")
            img = img_pat.sub("http://yoda-2f8a.kxcdn.com", img)
            src_pat = re.compile("job.proof_img_url")
            content = src_pat.sub(img, content)
            rep_name_pat = re.compile("rep\.name")
            content = rep_name_pat.sub(rep_name, content)
            rep_email_pat = re.compile("rep\.email")
            content = rep_email_pat.sub(sender, content)
            designer_name_pat = re.compile("designer\.name")
            content = designer_name_pat.sub(artist, content)
            designer_email_pat = re.compile("designer\.email")
            content = designer_email_pat.sub(artist_email, content)
            cfgobj = DBSession.query(Config).filter(Config.name == u'SENDGRID_API_KEY').one()
            sg = sendgrid.SendGridAPIClient(apikey=cfgobj.value)

            data = {
                 "personalizations": [{
                    "to": [{
                        "email": recipient,
                        "name": contact
                     }],
                     "subject": subject,
                 }],
                 "from": {
                     "email": sender
                 },
                 "content": [{"type": "text/html",
                 "value": content}]
            }

            response = sg.client.mail.send.post(request_body=data)

            eml.sent = 1
            eml.status_id = 3
            sent_date = strftime('%-m/%-d/%-y')
            eml_log = EmailLog(ord_id=ord_obj.id, sender_id=ord_obj.Rep.user.user_id, tmplt_id=tmplt_obj.id,sent_date=sent_date, recips=recipient, subject=eml.subject, mesg=eml.mesg)
            DBSession.add(eml_log)
            DBSession.flush()

        return()

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def holdemlmsg(self, id, ord_id, status_id, *args, **kw):
        """Handle the hold email message."""

        #log.debug('ord_id = ' + ord_id + '  status_id = ' + status_id + '  id = ' + id)
        proof_obj = DBSession.query(EmailQueue).get(id) 
        proof_obj.status_id = status_id
        DBSession.flush()

        redirect("/job/?id=" + str(ord_id))

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def sendmarker(self, ret_id, customer, job_name, mkr_id, sender, recipient, *args, **kw):
        """Email marker request"""

        #from tgext.mailer import get_mailer
        #mailer = get_mailer(request)

        #message = Message(subject="New Marker Request for " + job_name,
                  #sender=sender,
                  #recipients=[recipient],
                  #body='There is a new marker request for <a href=\"10.1.10.250:8080//job?id=' + ret_id + '\"!>' + job_name + '</a>')

        #mailer.send(message)

        cfgobj = DBSession.query(Config).filter(Config.name == u'art_mgr').one()
        art_cc = cfgobj.value
        if art_cc == recipient:
            art_cc = None

        cfgobj = DBSession.query(Config).filter(Config.name == u'SENDGRID_API_KEY').one()

        sg = sendgrid.SendGridAPIClient(apikey=cfgobj.value)
        subject = "New Marker Request for " + job_name
        content = '<p>There is a new marker request for <a href=\"http://10.1.10.250:8080/job?id=' + ret_id + '\">Internal link for ' + customer + job_name + '</a></p><p>There is a new marker request for <a href=\"https://75.148.95.30/job?id=' + ret_id + '\">External link for ' + customer + job_name + '</a></p>'
        #log.debug("content = " + content)
        #log.debug(recipient)

        if art_cc:
            data = { "personalizations": [
                   { "to": [
                       { "email": recipient}],
                     "cc": [{"email": art_cc}], 
                     "subject": subject}],
                     "from": {"email": sender},
                     "content": [{"type": "text/html",
                     "value": content}]
            }
        else:
            data = { "personalizations": [
                   { "to": [
                       { "email": recipient}],
                     "subject": subject}],
                     "from": {"email": sender},
                     "content": [{"type": "text/html",
                     "value": content}]
            }

        response = sg.client.mail.send.post(request_body=data)
        #log.debug(response.status_code)
        #log.debug(response.body)
        #log.debug(response.headers)

        mkr_obj = DBSession.query(MarkerRequest).get(mkr_id)
        sender_id = request.identity['repoze.who.userid']
        sent_date = strftime('%-m/%-d/%-y')
        eml_log = EmailLog(ord_id=mkr_obj.ord_id, sender_id=sender_id, tmplt_id=9999,sent_date=sent_date, recips=recipient, subject=subject, mesg=content)
        DBSession.add(eml_log)
        ord_obj = DBSession.query(Order).get(mkr_obj.ord_id)
        if mkr_obj.new == 1:
            mkr_obj.new = 2 

        dte = strftime('%-m/%-d/%-y') 
        mkr_log = MarkerRequestLog(ord_id=mkr_obj.ord_id,marker_id=mkr_id, date_in=dte, new=mkr_obj.new)
        DBSession.add(mkr_log)
        DBSession.flush()

        flash("Marker Request sent!", "success")

        redirect("/job?id=" + ret_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def emailproof(self, ret_id, cust_name, contact, job_name, rep_name, job_num, artist, artist_email, proof, sender, recipient, *args, **kw):
        """Email proof to customer"""

        import re
        #import base64
        #from tgext.mailer import get_mailer
        #from tgext.mailer import Attachment
        #mailer = get_mailer(request)
        #html = Attachment(data="<html><p>Proof</p></html>",
                  #transfer_encoding="quoted-printable")
        #body = Attachment(data="This is your proof from No Dinx",
                  #transfer_encoding="quoted-printable")

        #message = Message(subject="Proof from No Dinx",
                  #sender=sender,
                  #recipients=[recipient],
                  #html=html, 
                  #body=body)

        #mailer.send(message)

        rep = sender.split("@")[0].capitalize()
        fullpath = 'ND-TEMPLATE.html'  # this needs to come from the config table
        eml_tmp = open(str(fullpath).encode('utf-8'), 'r+')
        content = eml_tmp.read()
        eml_tmp.close()
        cust_name_pat = re.compile("customer\.name")
        content = cust_name_pat.sub(cust_name, content)
        cust_email = re.compile("customer\.email")
        content = cust_email.sub(recipient, content)
        cust_contact_pat = re.compile("customer\.contact") 
        content = cust_contact_pat.sub(contact, content)
        job_name_pat = re.compile("job\.name")
        content = job_name_pat.sub(job_name, content)
        job_id_pat = re.compile("job\.id")
        content = job_id_pat.sub(ret_id, content)
        #job_proof_pat = re.compile("job\.proof")
        #img_file = open("nodinxprd/public" + str(proof), "rb")
        #encd_img = base64.b64encode(img_file.read())
        #content = job_proof_pat.sub(encd_img, content)
        proof_path_pat = re.compile("\/res/Customer_jobs")
        #proof = proof_path_pat.sub("", proof)
        proof = proof_path_pat.sub("http://yoda-2f8a.kxcdn.com", proof)
        src_pat = re.compile("job.proof_img_url")
        content = src_pat.sub(proof, content)
        rep_name_pat = re.compile("rep\.name")
        content = rep_name_pat.sub(rep_name, content)
        rep_email_pat = re.compile("rep\.email")
        content = rep_email_pat.sub(sender, content)
        designer_name_pat = re.compile("designer\.name")
        content = designer_name_pat.sub(artist, content)
        designer_email_pat = re.compile("designer\.email")
        content = designer_email_pat.sub(artist_email, content)

        cfgobj = DBSession.query(Config).filter(Config.name == u'SENDGRID_API_KEY').one()

        sg = sendgrid.SendGridAPIClient(apikey=cfgobj.value)
        subject = "No Dinx has a proof for you to review!" 
        """
        data = { "attachments": [{
                     "content": encd_img, 
                     "content_id": "proofxx", 
                     "disposition": "inline", 
                     "filename": "proof.png", 
                     "name": "proof", 
                     "type": "png"
                   }],
        """
        data = {
                 "personalizations": [{
                    "to": [{
                        "email": recipient,
                        "name": contact
                     }],
                     "subject": subject,
                     "cc": [{
                         "email": sender, 
                         "name": "Me" 
                     }]
                 }],
                 "from": {
                     "email": sender
                 },
                 "content": [{"type": "text/html",
                 "value": content}]
        }
        response = sg.client.mail.send.post(request_body=data)
        sender_id = request.identity['repoze.who.userid']
        sent_date = strftime('%-m/%-d/%-y')
        eml_log = EmailLog(ord_id=ret_id, sender_id=sender_id, tmplt_id=9998, sent_date=sent_date, recips=recipient, subject=subject, mesg=content)
        DBSession.add(eml_log)
        DBSession.flush()
        #log.debug(response.status_code)

        flash("Proof sent to " + recipient  + " !", "success")

        redirect("/job?id=" + ret_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def proofready(self, ret_id, customer, job_name, mkr_id, sender, recipient, *args, **kw):
        """Email proof ready notice"""

        #from tgext.mailer import get_mailer
        #mailer = get_mailer(request)

        #message = Message(subject="Proof for " + job_name + " is ready!",
                  #sender=sender,
                  #recipients=[recipient],
                  #body="Proof ready for !")

        #mailer.send(message)

        cfgobj = DBSession.query(Config).filter(Config.name == u'SENDGRID_API_KEY').one()

        sg = sendgrid.SendGridAPIClient(apikey=cfgobj.value)
        subject = "Proof ready for " + customer + job_name
        content = '<p>The proof for  <a href=\"http://10.1.10.250:8080/job?id=' + ret_id + '\">' + customer + job_name + ' Internal link</a>' + ' is ready for review.</p><p>The proof for  <a href=\"https://75.148.95.30/job?id=' + ret_id + '\">' + customer + job_name + ' External link</a>' + ' is ready for review.</p>'
        data = { "personalizations": [
                   { "to": [
                       { "email": recipient}],
                     "subject": subject}],
                     "from": {"email": sender},
                     "content": [{"type": "text/html",
                     "value": content}]
        }
        response = sg.client.mail.send.post(request_body=data)
        sent_date = strftime('%-m/%-d/%-y')
        eml_log = EmailLog(ord_id=ret_id, sender_id=sender, tmplt_id=9997, sent_date=sent_date, recips=recipient, subject=subject, mesg=content)
        DBSession.add(eml_log)
        DBSession.flush()
        #log.debug(response.status_code)

        flash("Proof ready email sent!", "success")

        redirect("/job?id=" + ret_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def requestjobnum(self, custname, job_name, sender, job_id, ae_num, msg, *args, **kw):
        """Email job number request to front office staff"""

        email_list = []
        recip_list = DBSession.query(Config).filter(Config.name == u'front_office').one().value.split(',')
        email_str = ''
        for email in recip_list:
            email_list.append({"email": email})
            email_str = email_str + ', ' + email
        email_str = email_str[2:]
        cfgobj = DBSession.query(Config).filter(Config.name == u'SENDGRID_API_KEY').one()

        sg = sendgrid.SendGridAPIClient(apikey=cfgobj.value)
        subject = 'The following job needs a job number: Customer=' + ae_num + ' - ' + job_name
        content = 'The job  <a href=\"http://10.1.10.250:8080/job?id=' + job_id + '\">' + custname + ' - ' + job_name + '</a> needs a job number.       ' + msg
        data = { "personalizations": [
                   { "to": email_list,
                       
                     "subject": subject}],
                     "from": {"email": sender},
                     "content": [{"type": "text/html",
                     "value": content}]
        }
        response = sg.client.mail.send.post(request_body=data)
        sent_date = strftime('%-m/%-d/%-y')
        eml_log = EmailLog(ord_id=job_id, sender_id=sender, tmplt_id=9994, sent_date=sent_date, recips=email_str, subject=subject, mesg=content)
        DBSession.add(eml_log)
        DBSession.flush()
        #log.debug(response.status_code)

        flash("Job number request sent!", "success")

        redirect("/job?id=" + job_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def requestinvoice(self, custname, job_name, sender, job_id, ae_num, msg, *args, **kw):
        """Email job number request to front office staff"""

        email_list = []
        email_str = ''
        recip_list = DBSession.query(Config).filter(Config.name == u'front_office').one().value.split(',')
        for email in recip_list:
            email_list.append({"email": email})
            email_str = email_str + ', ' + email
        email_str = email_str[2:]
        cfgobj = DBSession.query(Config).filter(Config.name == u'SENDGRID_API_KEY').one()

        sg = sendgrid.SendGridAPIClient(apikey=cfgobj.value)
        subject = 'The following job needs to be invoiced: Customer = ' + ae_num + ' - ' + 'Job Name = ' + job_name
        content = 'The job  <a href=\"http://10.1.10.250:8080/job?id=' + job_id + '\">' + custname + ' - ' + job_name + '</a> needs to be invoiced.      <p>' + msg
        data = { "personalizations": [
                   { "to": email_list,
                       
                     "subject": subject}],
                     "from": {"email": sender},
                     "content": [{"type": "text/html",
                     "value": content}]
        }
        response = sg.client.mail.send.post(request_body=data)
        sent_date = strftime('%-m/%-d/%-y')
        eml_log = EmailLog(ord_id=job_id, sender_id=sender, tmplt_id=9996, sent_date=sent_date, recips=email_str, subject=subject, mesg=content)
        DBSession.add(eml_log)
        DBSession.flush()

        flash("Invoice requested!", "success")

        redirect("/job?id=" + job_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def emailupdate(self, custname, job_name, sender, job_id, msg, recip_list, *args, **kw):
        """Email job update message to office staff"""

        if sender:
           sender_obj = DBSession.query(User).filter(User.user_name == sender.strip()).first()
        email_list = []
        
        for name in recip_list.split(','):
            email = DBSession.query(User).filter(User.user_name == name.strip()).first().email_address
            email_list.append({"email": email})
        cfgobj = DBSession.query(Config).filter(Config.name == u'SENDGRID_API_KEY').one()

        sg = sendgrid.SendGridAPIClient(apikey=cfgobj.value)
        subject = 'Important update for Customer: ' + custname + ' and job: ' + job_name
        content = '<a href=\"http://10.1.10.250:8080/job?id=' + job_id + '\"> The job for customer: ' + custname + ' - Job: ' + job_name + '</a><p>' + msg
        data = { "personalizations": [
                   { "to": email_list,
                     "subject": subject}],
                     "from": {"email": sender_obj.email_address},
                     "content": [{"type": "text/html",
                     "value": content}]
        }
        response = sg.client.mail.send.post(request_body=data)
        sent_date = strftime('%-m/%-d/%-y')
        eml_log = EmailLog(ord_id=job_id, sender_id=sender, tmplt_id=9995, sent_date=sent_date, recips=recip_list, subject=subject, mesg=content)
        DBSession.add(eml_log)
        DBSession.flush()
        #log.debug(response.status_code)

        flash("Job update message sent!", "success")

        redirect("/job?id=" + job_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def revision(self, ret_id, job_name, mkr_id, sender, recipient, *args, **kw):
        """Email revision request notice."""

        from tgext.mailer import get_mailer
        mailer = get_mailer(request)

        message = Message(subject="Revision Request for " + job_name,
                  sender="sender",
                  recipients=[recipient],
                  body="Revision requested for " + job_name + " !")

        mailer.send(message)

        mkr_obj = DBSession.query(MarkerRequest).get(mkr_id)

        if mkr_obj:
            mkr_obj.new += 1

        flash("Revision Request sent!", "success")

        redirect("/job?id=" + ret_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def pushquote(self, ret_id, *args, **kw):
        """Push quote to detail line/s"""

        det_objs = DBSession.query(OrderDetail).filter(OrderDetail.ord_id == ret_id).filter(OrderDetail.pushed == False).all()
        for q_det in det_objs:
            q_det.pushed = True
            for option in q_det.options:
                option.pushed=True

        flash("Quote pushed!", "success")

        redirect("/job?id=" + ret_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def addtowk(self, ret_id, *args, **kw):
        """Add order details to po detail line/s"""

        today  = strftime('%-m/%-d/%y')

        wk_obj = DBSession.query(Worksheet).filter(Worksheet.frozen == False).first()
        if not wk_obj:
            cnt = DBSession.query(Worksheet).filter(Worksheet.desc == today).count()
            wk_obj = Worksheet(desc=today + str(cnt + 1))
            DBSession.add(wk_obj)
            DBSession.flush()

        det_objs = DBSession.query(OrderDetail).filter(OrderDetail.ord_id == ret_id).filter(OrderDetail.on_order == False).all()

        if not det_objs:
            flash("No details found!", "warning")
            redirect("/job?id=" + ret_id)

        for q_det in det_objs:
            #if q_det.item.desc == u'' or q_det.item.desc is None or q_det.on_order:
            if q_det.item.v_item == u'' or q_det.item.v_item is None or q_det.on_order or q_det.item.color == u'' or q_det.item.color is None:
                continue
            q_det.on_order = True
            wk_det_obj = WorksheetDetail(wk_id=wk_obj.id, ordd_id=q_det.id, hot=0, allocated=0, item_id=q_det.item_id, qty_xs=q_det.qty_xs, qty_sm=q_det.qty_sm, qty_md=q_det.qty_md, qty_lg=q_det.qty_lg, qty_xl=q_det.qty_xl, qty_2xl=q_det.qty_2xl, qty_3xl=q_det.qty_3xl, qty_4xl=q_det.qty_4xl, qty_misc=q_det.qty_misc, change_dtim=today)
            DBSession.add(wk_det_obj)
            q_det.on_order = True
            DBSession.flush()

        DBSession.flush()

        flash("Items addded to Worksheet!", "success")

        redirect("/job?id=" + ret_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def delete(self, ret_page, id, ret_id, obj, *args, **kw):
        """Handle deletes"""

        if obj == 'OrderDetail':
            options = DBSession.query(OrderOption).filter(OrderOption.ordd_id == id).all()
            for option in options:
                DBSession.delete(option)
            for wkshdet in DBSession.query(WorksheetDetail).filter(WorksheetDetail.ordd_id == id).all():
                DBSession.delete(wkshdet)
            del_obj = DBSession.query(OrderDetail).get(id)
        if obj == 'Address':
            del_obj = DBSession.query(Address).get(id)
        if obj == 'PODetail':
            del_obj = DBSession.query(PODetail).get(id)
        if obj == 'OrderOption':
            del_obj = DBSession.query(OrderOption).get(id)
        """
        if obj == 'MarkerRequest':
            del_obj = DBSession.query(MarkerRequest).get(id)
            proof = DBSession.query(Proof).get(del_obj.proof_id)
            DBSession.delete(proof)
            for mkr in DBSession.query(MarkerRequest).filter(MarkerRequest.id != id).filter(MarkerRequest.cnt > del_obj.cnt).filter(MarkerRequest.ord_id == del_obj.ord_id).all():
                mkr.cnt -= 1
        """
        if obj == 'Order':
            del_obj = DBSession.query(Order).get(id)
            mrk = DBSession.query(MarkerRequest).get(del_obj.marker_id)
            DBSession.delete(mrk)
            mrklog = DBSession.query(MarkerRequestLog).filter(MarkerRequestLog.ord_id == del_obj.id).first()
            if mrklog:
                DBSession.delete(mrklog)
            for det in del_obj.details:
                for opt in det.options:
                    DBSession.delete(opt)
                DBSession.delete(det)
            for proof in DBSession.query(Proof).filter(Proof.marker_id == del_obj.marker_id).all():
                DBSession.delete(proof)
            #for det in del_obj.details:
            #    for podet in DBSession.query(PODetail).filter(PODetail.ordd_id == det.id):
            #        DBSession.delete(podet)
            #    DBSession.delete(det)
        if obj == 'WorksheetDetail':
            del_obj = DBSession.query(WorksheetDetail).get(id)
        if obj == 'Proof':
            del_obj = DBSession.query(Proof).get(id)
            import re
            temp_pat = re.compile('\/img\/proof_tmp.png')
            if not temp_pat.match(del_obj.proof_path):
                try:
                    fullpath = os.getcwd() + '/nodinxprd/public' + del_obj.proof_path
                    os.remove(fullpath)
                except:
                    log.debug("proof file was deleted outside of the application  " + fullpath)
        if obj == 'Project':
            del_obj = DBSession.query(Project).get(id)
            for job in DBSession.query(Order).filter(Order.proj_id == id):
                job.proj_id = None
                DBSession.flush()

        if obj == 'EmailQueue':
            del_obj = DBSession.query(EmailQueue).get(id)
            
        DBSession.delete(del_obj)
        DBSession.flush()

        flash(obj + " deleted!", "success")

        if obj == 'Order':
            redirect("/dashboard/?view=active")
        elif obj == 'Project':
            redirect("/projects")
        else:
            redirect("/" + ret_page + "/?id=" + ret_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setcustname(self, cust_id, name, **kw):
        try:
            if cust_id:
               Object = DBSession.query(Customer).get(cust_id)
               Object.name = name
               DBSession.flush()

        except():
            flash("Error saving edit!", "danger")
            redirect("/" + page + "?id=" + ret_id)

        flash("Success updating customer for " + str(Object.job_name) + "!", "success")

        redirect("/job/?id=" + job_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setgetcust(self, job_id, cust_id, **kw):
        try:
            if cust_id:
               Object = DBSession.query(Order).get(job_id)
               Object.cust_id = cust_id
               DBSession.flush()

        except():
            flash("Error saving edit!", "danger")
            redirect("/" + page + "?id=" + ret_id)

        flash("Success updating customer for " + str(Object.job_name) + "!", "success")

        redirect("/job/?id=" + job_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setgetproj(self, job_id, proj_id, **kw):
        try:
            if job_id:
               Object = DBSession.query(Order).get(job_id)
               Object.proj_id = proj_id
               DBSession.flush()

        except():
            flash("Error adding job to project!", "danger")

        flash("Success adding job to project!", "success")

        redirect("/job/?id=" + job_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setgetitemjob(self, job_id, det_id, item_id, **kw):
        try:
            if det_id:
               Object = DBSession.query(OderDetail).get(det_id)
               Object.item_id = item_id
               DBSession.flush()

        except():
            flash("Error saving edit!", "danger")

        flash("Success updating order detail item!", "success")

        redirect("/job/?id=" + job_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setitemqty(self, id, qty=1, **kw):
        try:
            if id:
                Object = DBSession.query(OrderDetail).get(id)
                Object.qty_misc = qty
                DBSession.flush()

        except():
            flash("Error saving edit!", "danger")
            redirect("/dashboard/")

        #flash("Success updating order detail item!", "success")

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setgetitemdetail(self, det_id, item_id, **kw):
        try:
            if det_id:
                Object = DBSession.query(OrderDetail).get(det_id)
                Object.item_id = item_id
                DBSession.flush()

        except():
            flash("Error saving edit!", "danger")
            redirect("/orddetail/?id=" + det_id)

        flash("Success updating order detail item!", "success")

        redirect("/orddetail/?id=" + det_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def addline(self, po_id, cnt, **kw):
        try:
            po_obj = DBSession.query(PO).get(po_id)
            if po_obj.closed:
                flash("You can't add lines to a closed PO!", "warning")
                redirect("/purchase/?id=" + po_id)
            end = int(cnt) + 1
            for num in range(1, end):
                po_det = PODetail(po_id=po_id)
                #log.debug('po_id = ' + str(po_id))
                DBSession.add(po_det)
                DBSession.flush()

        except():
            flash("Error adding PO lines!", "danger")
            redirect("/purchase/?id=" + po_id)

        flash("Success adding PO detail lines!", "success")

        redirect("/purchase/?id=" + po_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def additem(self, job_id, cnt, **kw):
        try:
            end = int(cnt) + 1
            for num in range(1, end):
                item = Item(type=20)
                #log.debug('item id = ' + str(item.id))
                DBSession.add(item)
                DBSession.flush()
                Object = OrderDetail(ord_id=job_id, item_id=item.id, pushed=False, on_order=False)
                DBSession.add(Object)
                DBSession.flush()

        except():
            flash("Error saving edit!", "danger")
            redirect("/job/?id=" + job_id)

        flash("Success adding order detail item!", "success")

        redirect("/job/?id=" + job_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def addoitem(self, job_id, item_id, **kw):
        try:
            if item_id:
                Object = OrderDetail(ord_id=job_id, item_id=item_id, pushed=False, on_order=False, qty_misc=1)
                DBSession.add(Object)
                DBSession.flush()
            else:
                flash("Missing item!", "warning")

        except():
            flash("Error saving edit!", "danger")
            redirect("/job/?id=" + job_id)

        flash("Success adding order detail item!", "success")

        redirect("/job/?id=" + job_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def dupjob(self, job_id, **kw):
        try:
            if job_id:
                ord_dte = strftime('%-m/%-d')
                old_ord = DBSession.query(Order).get(job_id)
                job_name = "Copy of " + old_ord.job_name
                new_ord = Order(cust_id=old_ord.cust_id, job_name=job_name, job_num=old_ord.job_num, prod_apprv=2, rep_id=1, order_date=ord_dte, ord_type_id = old_ord.ord_type_id, ord_stat_id=1, thnks_sent=2, rush=2, hold=2, cust_apprv=2, est=old_ord.est, ship_pkup=old_ord.ship_pkup, ship_type_id=old_ord.ship_type_id, prod_notes=old_ord.prod_notes, prod_res=old_ord.prod_res, charity=old_ord.charity)
                DBSession.add(new_ord)
                DBSession.flush()

                if old_ord.ord_type_id in (1,2):
                    for old_det in old_ord.details:
                        new_item = Item(v_item=old_det.item.v_item, color=old_det.item.color, desc=old_det.item.desc, type=old_det.item.type)
                        DBSession.add(new_item)
                        DBSession.flush()
                        new_det = OrderDetail(ord_id=new_ord.id, item_id=new_item.id, qty_xs=old_det.qty_xs, qty_sm=old_det.qty_sm, qty_md=old_det.qty_md, qty_lg=old_det.qty_lg, qty_xl=old_det.qty_xl, qty_2xl=old_det.qty_2xl, qty_3xl=old_det.qty_3xl, qty_4xl=old_det.qty_4xl, qty_misc=old_det.qty_misc, price=old_det.price)
                        DBSession.add(new_det)
                        DBSession.flush()
                else:
                    for old_det in old_ord.details:
                        new_det = OrderDetail(ord_id=new_ord.id, item_id=old_det.item_id, qty_misc=old_det.qty_misc, price=old_det.price)
                        DBSession.add(new_det)
                        DBSession.flush()
                        for old_opt in old_det.options:
                            new_opt = OrderOption(ordd_id=new_det.id, opt_id=old_opt.opt_id, price=old_opt.price, qty=old_opt.qty)
                            DBSession.add(new_opt)
                            DBSession.flush()
                        
                DBSession.flush()

                #old_mkrs = DBSession.query(MarkerRequest).filter(MarkerRequest.ord_id == old_ord.id).all()
                old_mkr = DBSession.query(MarkerRequest).get(old_ord.marker_id)
                new_mkr = MarkerRequest(ord_id=new_ord.id, status_id=1, artist_id=1, shirts=old_mkr.shirts, colors=old_mkr.colors, copy=old_mkr.copy, idea=old_mkr.idea, notes=old_mkr.notes, loc_1=old_mkr.loc_1, loc_1_color=old_mkr.loc_1_color, loc_2=old_mkr.loc_2, loc_2_color=old_mkr.loc_2_color, loc_3=old_mkr.loc_3, loc_3_color=old_mkr.loc_3_color, rev_num=old_mkr.rev_num, cnt=old_mkr.cnt)
                DBSession.add(new_mkr)
                DBSession.flush()
                new_ord.marker_id = new_mkr.id
                for proof in old_mkr.proofs:
                    new_proof = Proof(proof_path=proof.proof_path, marker_id=new_mkr.id)
                    DBSession.add(new_proof)
                    DBSession.flush()
            else:
                flash("Encountered an error while duplicating the order.", "warning")

        except():
            flash("Error duplicating order!", "danger")
            redirect("/job/?id=" + job_id)

        flash("Success duplicating order!", "success")

        redirect("/job/?id=" + str(new_ord.id))

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def addoption(self, det_id, job_id, opt_id, qty, **kw):
        try:
            if opt_id:
                #log.debug("opt_id = " + opt_id)
                opt_obj = DBSession.query(OptionType).get(opt_id)
                size = kw.get('size')
                if size:
                    description = opt_obj.desc + ' ' + str(size) + ' sqft'
                else:
                    description = opt_obj.desc
                Object = OrderOption(ordd_id=det_id, opt_id=opt_id, desc=description, pushed=False, active=True, qty=qty, price=opt_obj.price)
                DBSession.add(Object)
                DBSession.flush()
            else:
                flash("No option was selected!", "warning")
                redirect("/job/?id=" + job_id)
                #redirect("/getoptions/?job_id=" + job_id + "&amp;item_id=" + item_id)

        except():
            flash("Error adding options!", "danger")
            redirect("/job/?id=" + job_id)

        flash("Success adding order detail option!", "success")

        #redirect("/getoptions/?ord_id=" + job_id + "&amp;det_id=" + det_id + "&amp;item_id=" + item_id)
        redirect("/job/?id=" + job_id)

    #--->

    @expose('nodinxprd.templates.getoptions')
    @require(predicates.not_anonymous())
    def getoptions(self, ord_id, det_id, item_id, **kw):
        try:
            if item_id:
                obj_item = DBSession.query(Item).get(item_id)
                if det_id:
                    catg_list = []
                    opt_data = DBSession.query(OrderOption).filter(OrderOption.ordd_id == det_id).all()
                    for opt in opt_data:
                        catg_list.append(opt.type.catg_id)
                    opt_type_data = DBSession.query(OptionType).filter(OptionType.id == ItemOption.opt_type_id).filter(~OptionType.catg_id.in_(catg_list)).filter(ItemOption.item_type_id == obj_item.type).order_by(OptionType.id).all()
            else:
                flash("Unable to find options for the item selected.", "warning")
        except():
            flash("Error getting options!", "danger")
            redirect("/job/?id=" + job_id)

        if not opt_type_data:
            flash("No more options for the item selected.", "warning")

        objtype = "OrderOption"

        return dict(page='getoptions', opt_data=opt_type_data, item_id=obj_item.id, det_id=det_id, ord_id=ord_id, objtype=objtype)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setgetitempod(self, det_id, item_id, **kw):
        try:
            if det_id:
               Object = DBSession.query(PODetail).get(det_id)
               Object.item_id = item_id
               DBSession.flush()

        except():
            flash("Error saving edit!", "danger")
            redirect("/podetail/?id=" + det_id)

        flash("Success updating po detail item!", "success")

        redirect("/podetail/?id=" + det_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setgetvendor(self, pod_id, vend_id, item_id, **kw):
        try:
            if pod_id:
               Object = DBSession.query(PODetail).get(pod_id)
               Object.vendor = vend_id
               DBSession.flush()

        except():
            flash("Error updating vendor!", "danger")

        flash("Success updating po detail vendor!", "success")

        redirect("/podetail/?id=" + pod_id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setproof(self, id, path='/img/proof_tmp.png', **kw):
        try:
            if id:
               Object = DBSession.query(Proof).get(id)
               Object.proof_path = path
               DBSession.flush()

        except():
            flash("Error updating proof path!", "danger")

        flash("Success updating proof path!", "success")

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setprodnotes(self, id, prod_notes, numsheet='', **kw):
        try:
            if id:
                Object = DBSession.query(Order).get(id)
                Object.prod_notes = prod_notes
                Object.numsheet = numsheet
                DBSession.flush()

                flash("Success updating production notes!", "success")
                redirect('/job/?id=' + str(id))

        except():
            flash("Error updating production notes!", "danger")
            redirect('/job/?id=' + str(id))

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setcntprint(self, det_id, field, val, **kw):
        try:
            if det_id:
               Object = DBSession.query(OrderDetail).get(det_id)
               if field == 'cnt_xs':
                   Object.cnt_xs = val
               elif field == 'cnt_sm':
                   Object.cnt_sm = val
               elif field == 'cnt_md':
                   Object.cnt_md = val
               elif field == 'cnt_lg':
                   Object.cnt_lg = val
               elif field == 'cnt_xl':
                   Object.cnt_xl = val
               elif field == 'cnt_2xl':
                   Object.cnt_2xl = val
               elif field == 'cnt_3xl':
                   Object.cnt_3xl = val
               elif field == 'cnt_4xl':
                   Object.cnt_4xl = val
               elif field == 'cnt_misc':
                   Object.cnt_misc = val
               elif field == 'prnt_xs':
                   Object.prnt_xs = val
               elif field == 'prnt_sm':
                   Object.prnt_sm = val
               elif field == 'prnt_md':
                   Object.prnt_md = val
               elif field == 'prnt_lg':
                   Object.prnt_lg = val
               elif field == 'prnt_xl':
                   Object.prnt_xl = val
               elif field == 'prnt_2xl':
                   Object.prnt_2xl = val
               elif field == 'prnt_3xl':
                   Object.prnt_3xl = val
               elif field == 'prnt_4xl':
                   Object.prnt_4xl = val
               elif field == 'prnt_misc':
                   Object.prnt_misc = val
               DBSession.flush()

        except():
            flash("Error updating count or print!", "danger")

        #flash("Success updating count/print!", "success")
        return()

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setoptval(self, opt_id, field, val, **kw):
        try:
            if opt_id:
               Object = DBSession.query(OrderOption).get(opt_id)
               if field == 'qty':
                   Object.qty = val
               if field == 'price':
                   Object.price = val
               if field == 'desc':
                   Object.desc = val
               DBSession.flush()

        except():
            flash("Error updating qty!", "danger")

        #flash("Success updating qty!", "success")
        return()

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setarchive(self, ord_id, **kw):
        try:
            if ord_id:
               Object = DBSession.query(Order).get(ord_id)
               #log.debug("archive value = " + str(Object.archived))
               if Object.archived:
                   Object.archived = False
               else:
                   Object.archived = True
               DBSession.flush()

        except():
            flash("Error setting archive status!", "danger")

        #flash("Success setting archive status!", "success")
        return()

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def newemail(self, ord_id, tmplt_id, send_date, queue_date, send_offset, subject, mesg, emlstat_id, **kw):
        try:
            if ord_id:
               if not tmplt_id or not send_date or not queue_date or not send_offset or not emlstat_id:
                   flash("Error creating email!", "warning")
                   redirect("/job/?id=" + str(ord_id))
               if emlstat_id == 'q':
                   Object = EmailQueue(ord_id=ord_id, tmplt_id=tmplt_id, status_id=1, send_date=send_date, queue_date=queue_date, subject=subject, mesg=mesg, send_offset=send_offset, sent=0)
                   DBSession.add(Object)
                   DBSession.flush()
                   flash("Email Queued!", "success")
               elif emlstat_id == 'h':
                   Object = EmailQueue(ord_id=ord_id, tmplt_id=tmplt_id, status_id=2, send_date=send_date, queue_date=queue_date, subject=subject, mesg=mesg, send_offset=send_offset, sent=0)
                   DBSession.add(Object)
                   DBSession.flush()
                   flash("Email Held!", "success")
               #else:
                   #Object = EmailQueue(ord_id=ord_id, tmplt_id=tmplt_id, status_id=1, send_date=send_date, queue_date=queue_date, subject=subject, mesg=mesg, send_offset=send_offset, sent=0)
                   #DBSession.add(Object)
                   #DBSession.flush()
                   #flash("Email Sent!", "success")

        except():
            flash("Error creating email!", "danger")

        redirect("/job/?id=" + str(ord_id))

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def openpo(self, id, **kw):
        try:
            if id:
               Object = DBSession.query(PO).get(id)
               if Object.closed:
                   Object.closed = False
               DBSession.flush()

        except():
            flash("Error closing PO status!", "danger")

        flash("Success closing PO status!", "success")

        redirect("/purchase/?id=" + id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def closepo(self, id, **kw):
        try:
            if id:
               Object = DBSession.query(PO).get(id)
               if not Object.closed:
                   Object.closed = True
               DBSession.flush()

        except():
            flash("Error closing PO status!", "danger")

        flash("Success closing PO status!", "success")

        redirect("/purchase/?id=" + id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def freeze(self, id, **kw):
        try:
            if id:
               Object = DBSession.query(Worksheet).get(id)
               if Object.frozen:
                   Object.frozen = False
               else:
                   Object.frozen = True
               DBSession.flush()

        except():
            flash("Error setting frozen status!", "danger")

        flash("Success setting frozen status!", "success")

        redirect("/worksheet/?id=" + id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def genpo(self, id, **kw):
        try:
            if id:
               det_data = DBSession.query(WorksheetDetail).filter(WorksheetDetail.wk_id == id).filter(WorksheetDetail.allocated == False).order_by(WorksheetDetail.id).all()
               if len(det_data) < 1:
                   flash("All detail rows have been allocated", "warning")
                   redirect("/worksheet/?id=" + id)
               today  = strftime('%-m/%-d/%y') + '-' +str(id)
               po_obj = PO(num=today)
               DBSession.add(po_obj)
               pod_arr = []
               for detail in det_data:
                   #log.debug("Top level Item" +  detail.orddet.item.v_item + '   Color:' + detail.orddet.item.color)
                   if (str(detail.orddet.item.v_item).lower().strip(), str(detail.orddet.item.color).lower().strip()) in pod_arr:
                       #log.debug('Found skipping' + detail.orddet.item.v_item + '    ' + detail.orddet.item.color)
                       continue
                   detail.allocated = True
                   pod_obj = PODetail(po_id=po_obj.id)
                   DBSession.add(pod_obj)

                   pod_obj.item = str(detail.orddet.item.v_item).lower().strip()
                   pod_obj.color = str(detail.orddet.item.color).lower().strip()
                   pod_obj.desc = detail.orddet.item.desc

                   if not pod_obj.wk_dets is None:
                       pod_obj.wk_dets =str(pod_obj.wk_dets) + ',' + str(detail.id)
                   else:
                       pod_obj.wk_dets = detail.id

                   m_qty_xs = detail.qty_xs
                   if m_qty_xs is None or m_qty_xs == '' or not m_qty_xs.isdigit:
                       m_qty_xs = 0
                   m_qty_sm = detail.qty_sm
                   if m_qty_sm is None or m_qty_sm == '' or not m_qty_sm.isdigit:
                       m_qty_sm = 0
                   m_qty_md = detail.qty_md
                   if m_qty_md is None or m_qty_md == '' or not m_qty_md.isdigit:
                       m_qty_md = 0
                   m_qty_lg = detail.qty_lg
                   if m_qty_lg is None or m_qty_lg == '' or not m_qty_lg.isdigit:
                       m_qty_lg = 0
                   m_qty_xl = detail.qty_xl
                   if m_qty_xl is None or m_qty_xl == '' or not m_qty_xl.isdigit:
                       m_qty_xl = 0
                   m_qty_2xl = detail.qty_2xl
                   if m_qty_2xl is None or m_qty_2xl == '' or not m_qty_2xl.isdigit:
                       m_qty_2xl = 0
                   m_qty_3xl = detail.qty_3xl
                   if m_qty_3xl is None or m_qty_3xl == '' or not m_qty_3xl.isdigit:
                       m_qty_3xl = 0
                   m_qty_4xl = detail.qty_4xl
                   if m_qty_4xl is None or m_qty_4xl == '' or not m_qty_4xl.isdigit:
                       m_qty_4xl = 0
                   m_qty_misc = detail.qty_misc
                   if m_qty_misc is None or m_qty_misc == '' or not m_qty_misc.isdigit:
                       m_qty_misc = 0

                   pod_obj.inbnd_date = u''
                   pod_arr.append((pod_obj.item, pod_obj.color))

                   for chk in det_data:
                       #log.debug("Inner level Item: " +  chk.orddet.item.v_item + "   Color: " + chk.orddet.item.color)
                       if pod_obj.item == str(chk.orddet.item.v_item).lower().strip() and detail.id != chk.id and pod_obj.color == str(chk.orddet.item.color).lower().strip():
                           chk.allocated = True
                           qty_xs = chk.qty_xs
                           if qty_xs is None or qty_xs == '' or not qty_xs.isdigit:
                               qty_xs = 0
                           qty_sm = chk.qty_sm
                           if qty_sm is None or qty_sm == '' or not qty_sm.isdigit:
                               qty_sm = 0
                           qty_md = chk.qty_md
                           if qty_md is None or qty_md == '' or not qty_md.isdigit:
                               qty_md = 0
                           qty_lg = chk.qty_lg
                           if qty_lg is None or qty_lg == '' or not qty_lg.isdigit:
                               qty_lg = 0
                           qty_xl = chk.qty_xl
                           if qty_xl is None or qty_xl == '' or not qty_xl.isdigit:
                               qty_xl = 0
                           qty_2xl = chk.qty_2xl
                           if qty_2xl is None or qty_2xl == '' or not qty_2xl.isdigit:
                               qty_2xl = 0
                           qty_3xl = chk.qty_3xl
                           if qty_3xl is None or qty_3xl == '' or not qty_3xl.isdigit:
                               qty_3xl = 0
                           qty_4xl = chk.qty_4xl
                           if qty_4xl is None or qty_4xl == '' or not qty_4xl.isdigit:
                               qty_4xl = 0
                           qty_misc = chk.qty_misc
                           if qty_misc is None or qty_misc == '' or not qty_misc.isdigit:
                               qty_misc = 0

                           m_qty_xs = int(qty_xs) + int(m_qty_xs)
                           m_qty_sm = int(qty_sm) + int(m_qty_sm)
                           m_qty_md = int(qty_md) + int(m_qty_md)
                           m_qty_lg = int(qty_lg) + int(m_qty_lg)
                           m_qty_xl = int(qty_xl) + int(m_qty_xl)
                           m_qty_2xl = int(qty_2xl) + int(m_qty_2xl)
                           m_qty_3xl = int(qty_3xl) + int(m_qty_3xl)
                           m_qty_4xl = int(qty_4xl) + int(m_qty_4xl)
                           m_qty_misc = int(qty_misc) + int(m_qty_misc)

                           if pod_obj.wk_dets > 0:
                               pod_obj.wk_dets =str(pod_obj.wk_dets) + ',' + str(detail.id)
                           else:
                               pod_obj.wk_dets = detail.id


                   if m_qty_xs == 0:
                       pod_obj.qty_xs = ''
                   else:
                       pod_obj.qty_xs = m_qty_xs
                   if m_qty_sm == 0:
                       pod_obj.qty_sm = ''
                   else:
                       pod_obj.qty_sm = m_qty_sm
                   if m_qty_md == 0:
                       pod_obj.qty_md = ''
                   else:
                       pod_obj.qty_md = m_qty_md
                   if m_qty_lg == 0:
                       pod_obj.qty_lg = ''
                   else:
                       pod_obj.qty_lg = m_qty_lg
                   if m_qty_xl == 0:
                       pod_obj.qty_xl = ''
                   else:
                       pod_obj.qty_xl = m_qty_xl
                   if m_qty_2xl == 0:
                       pod_obj.qty_2xl = ''
                   else:
                       pod_obj.qty_2xl = m_qty_2xl
                   if m_qty_3xl == 0:
                       pod_obj.qty_3xl = ''
                   else:
                       pod_obj.qty_3xl = m_qty_3xl
                   if m_qty_4xl == 0:
                       pod_obj.qty_4xl = ''
                   else:
                       pod_obj.qty_4xl = m_qty_4xl
                   if m_qty_misc == 0:
                       pod_obj.qty_misc = ''
                   else:
                       pod_obj.qty_misc = m_qty_misc
                   DBSession.flush()

                           
               DBSession.flush()

        except():
            flash("Error setting frozen status!", "danger")
            redirect("/worksheet/?id=" + id)

        flash("Success setting frozen status!", "success")

        redirect("/worksheet/?id=" + id)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setpodetail(self, det_id, field, val, **kw):
        try:
            if det_id:
               Object = DBSession.query(PODetail).get(det_id)
               if field == 'vendor_name':
                   Object.vendor = val
               elif field == 'qty_xs':
                   Object.qty_xs = val
               elif field == 'qty_sm':
                   Object.qty_sm = val
               elif field == 'qty_md':
                   Object.qty_md = val
               elif field == 'qty_lg':
                   Object.qty_lg = val
               elif field == 'qty_xl':
                   Object.qty_xl = val
               elif field == 'qty_2xl':
                   Object.qty_2xl = val
               elif field == 'qty_3xl':
                   Object.qty_3xl = val
               elif field == 'qty_4xl':
                   Object.qty_4xl = val
               elif field == 'qty_misc':
                   Object.qty_misc = val
               elif field == 'ship_from':
                   Object.ship_from = val
               elif field == 'inbnd_date':
                   Object.inbnd_date = val
                   for wksh_id in str(Object.wk_dets).split(","):
                       wksh_obj = DBSession.query(WorksheetDetail).get(wksh_id)
                       if wksh_obj and wksh_obj.ordd_id and val:
                           ord_obj = DBSession.query(Order).get(wksh_obj.orddet.ord_id)
                           month, day = val.split('/')
                           new_date = date(int(strftime('%-Y')), int(month), int(day))
                           if ord_obj.inputs > 1:
                               #log.debug("ord_obj.inputs = " + ord_obj.inputs)
                               year, month, day = ord_obj.inputs.split('-')
                               old_date = date(int(year), int(month), int(day))
                               if new_date > old_date:
                                   ord_obj.inputs = str(new_date)
                           else:
                               ord_obj.inputs = str(new_date)

               elif field == 'hot':
                   Object.hot = val
               elif field == 'item':
                   Object.item = val
               elif field == 'color':
                   Object.color = val
               elif field == 'desc':
                   Object.desc = val
               DBSession.flush()

        except():
            flash("Error updating po detail!", "danger")

        #flash("Success updating po detail!", "success")
        return()

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setwkdetail(self, det_id, field, val, **kw):
        try:
            if det_id:
               Object = DBSession.query(WorksheetDetail).get(det_id)
               if field == 'qty_xs':
                   Object.qty_xs = val
               elif field == 'qty_sm':
                   Object.qty_sm = val
               elif field == 'qty_md':
                   Object.qty_md = val
               elif field == 'qty_lg':
                   Object.qty_lg = val
               elif field == 'qty_xl':
                   Object.qty_xl = val
               elif field == 'qty_2xl':
                   Object.qty_2xl = val
               elif field == 'qty_3xl':
                   Object.qty_3xl = val
               elif field == 'qty_4xl':
                   Object.qty_4xl = val
               elif field == 'qty_misc':
                   Object.qty_misc = val
               elif field == 'hot':
                   Object.hot = val
               DBSession.flush()

        except():
            flash("Error updating po detail!", "danger")

        #flash("Success updating po detail!", "success")
        return()

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setdetval(self, det_id, field, val, **kw):
        try:
            val = val.replace('"',"\'\'")
            if det_id:
               Object = DBSession.query(OrderDetail).get(det_id)
               if field == 'qty_xs':
                   Object.qty_xs = val
               elif field == 'qty_sm':
                   Object.qty_sm = val
               elif field == 'qty_md':
                   Object.qty_md = val
               elif field == 'qty_lg':
                   Object.qty_lg = val
               elif field == 'qty_xl':
                   Object.qty_xl = val
               elif field == 'qty_2xl':
                   Object.qty_2xl = val
               elif field == 'qty_3xl':
                   Object.qty_3xl = val
               elif field == 'qty_4xl':
                   Object.qty_4xl = val
               elif field == 'qty_misc':
                   Object.qty_misc = val
               elif field == 'v_item':
                   Object.item.v_item = val
               elif field == 'color':
                   Object.item.color = val
               elif field == 'desc':
                   Object.item.desc = val
               elif field == 'price':
                   Object.price = val
               elif field == 'l1':
                   Object.l1 = val
               elif field == 'l2':
                   Object.l2 = val
               elif field == 'l3':
                   Object.l3 = val
               elif field == 'l4':
                   Object.l4 = val
               elif field == 'l5':
                   Object.l5 = val
               elif field == 'l6':
                   Object.l6 = val
               elif field == 'on_order':
                   if val == 1:
                       Object.on_order = 1
                   else:
                       Object.on_order = 0
               DBSession.flush()

        except():
            flash("Error updating qty!", "danger")

        #flash("Success updating qty!", "success")
        return()

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def setapprv(self, ord_id, field, val, **kw):
        try:
            if ord_id:
               Object = DBSession.query(Order).get(ord_id)
               new_date = strftime('%-m/%-d')
               if field == 'schd':
                   Object.int_apprv_schd = val
                   Object.int_apprv_schd_date = new_date
               if field == 'ord':
                   Object.int_apprv_ord = val
                   Object.int_apprv_ord_date = new_date
               if field == 'art':
                   Object.int_apprv_art = val
                   Object.int_apprv_art_date = new_date
               if field == 'cntd':
                   Object.int_apprv_cntd = val
                   Object.int_apprv_cntd_date = new_date
               if field == 'brnd':
                   Object.int_apprv_brnd = val
                   Object.int_apprv_brnd_date = new_date
               if field == 'invcd':
                   Object.int_apprv_invcd = val
                   Object.int_apprv_invcd_date = new_date
               if field == 'prdmgr':
                   Object.int_apprv_prdmgr = val
                   Object.int_apprv_prdmgr_date = new_date
               if field == 'prntr':
                   Object.int_apprv_prntr = val
                   Object.int_apprv_prntr_date = new_date
               if field == 'rep':
                   Object.int_apprv_rep = val
                   Object.int_apprv_rep_date = new_date
               if field == 'chkd':
                   Object.int_apprv_chkd = val
                   Object.int_apprv_chkd_date = new_date
               if field == 'pakd':
                   Object.int_apprv_pakd = val
                   Object.int_apprv_pakd_date = new_date
               if field == 'shpd':
                   Object.int_apprv_shpd = val
                   Object.int_apprv_shpd_date = new_date

        except():
            flash("Error updating count or print!", "danger")

        DBSession.flush()
        #log.debug('new_date = ' + new_date)
        #flash("Success updating count/print!", "success")
        return ()

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def save(self, page, obj, **kw):
        try:
            id = kw.get('id')
            if obj == 'OrderDetail':
                Object = DBSession.query(OrderDetail).get(kw.get('id'))
                Object.qty_xs = kw.get('qty_xs')
                Object.qty_sm = kw.get('qty_sm')
                Object.qty_md = kw.get('qty_md')
                Object.qty_lg = kw.get('qty_lg')
                Object.qty_xl = kw.get('qty_xl')
                Object.qty_2xl = kw.get('qty_2xl')
                Object.qty_3xl = kw.get('qty_3xl')
                Object.qty_4xl = kw.get('qty_4xl')
                Object.qty_misc = kw.get('qty_misc')
                Object.cnt_xs = kw.get('cnt_xs')
                Object.cnt_sm = kw.get('cnt_sm')
                Object.cnt_md = kw.get('cnt_md')
                Object.cnt_lg = kw.get('cnt_lg')
                Object.cnt_xl = kw.get('cnt_xl')
                Object.cnt_2xl = kw.get('cnt_2xl')
                Object.cnt_3xl = kw.get('cnt_3xl')
                Object.cnt_4xl = kw.get('cnt_4xl')
                Object.cnt_misc = kw.get('cnt_misc')
                Object.prnt_xs = kw.get('prnt_xs')
                Object.prnt_sm = kw.get('prnt_sm')
                Object.prnt_md = kw.get('prnt_md')
                Object.prnt_lg = kw.get('prnt_lg')
                Object.prnt_xl = kw.get('prnt_xl')
                Object.prnt_2xl = kw.get('prnt_2xl')
                Object.prnt_3xl = kw.get('prnt_3xl')
                Object.prnt_4xl = kw.get('prnt_4xl')
                Object.prnt_misc = kw.get('prnt_misc')
                Object.l1 = kw.get('l1')
                Object.l1_so = kw.get('l1_so')
                Object.l2 = kw.get('l2')
                Object.l2_so = kw.get('l2_so')
                Object.l3 = kw.get('l3')
                Object.l3_so = kw.get('l3_so')
                Object.l4 = kw.get('l4')
                Object.l4_so = kw.get('l4_so')
                Object.l5 = kw.get('l5')
                Object.l5_so = kw.get('l5_so')
                Object.l6 = kw.get('l6')
                Object.l6_so = kw.get('l6_so')
                Object.price = kw.get('price')
                Object.change_dtim = strftime('%-m/%-d')
                Object.item_id = kw.get('item')
                Object.item.color = kw.get('color')
                Object.item.desc = kw.get('desc')

            elif obj == 'Order':
                Object = DBSession.query(Order).get(kw.get('id'))
                if kw.get('cust_name_tb'):
                    val = kw.get('cust_name_tb')
                    val = val.replace('"',"")
                    val = val.replace("\'","")
                    val = val.replace("/","")
                    val = val.replace("\#","")
                    val = val.replace("[","")
                    val = val.replace("]","")
                    val = val.replace("{","")
                    val = val.replace("}","")
                    #log.debug('val = ' + val)
                    Object.Customer.name = val
                    #Object.Customer.name = kw.get('cust_name_tb')
                val = kw.get('job_name')
                val = val.replace('"',"")
                val = val.replace("\'","")
                val = val.replace("/","")
                val = val.replace("\#","")
                val = val.replace("[","")
                val = val.replace("]","")
                val = val.replace("{","")
                val = val.replace("}","")
                #log.debug('val = ' + val)
                Object.job_name = val
                #Object.job_name = kw.get('job_name')
                val = kw.get('job_num')
                val = val.replace("/","")
                Object.job_num = val 
                Object.Customer.ae_num = kw.get('ae_num')
                Object.Customer.contact = kw.get('contact')
                Object.Customer.email = kw.get('email')
                Object.Customer.phone = kw.get('phone')
                Object.rep_id = kw.get('accnt_rep')
                Object.rush = kw.get('rush')
                Object.order_date = kw.get('order_date')
                Object.proof_date = kw.get('proof_date')
                Object.due_date = kw.get('due_date')
                Object.hold = kw.get('hold')
                Object.job_time_est = kw.get('job_time_est')
                Object.thnks_sent = kw.get('thnks_sent')
                Object.cust_apprv = kw.get('cust_apprv')
                Object.ord_type_id = kw.get('order_type')
                Object.time_est = kw.get('time_est')
                Object.ord_stat_id = kw.get('ord_status')
                if int(kw.get('ord_status')) == 4:
                    now = strftime('%-m/%-d/%-y')
                    for tmplt in DBSession.query(EmailTemplate).filter(EmailTemplate.active == True).all():
                        send_date = date.today() + timedelta(days=tmplt.offset)
                        emlqueue = EmailQueue(ord_id=Object.id, tmplt_id=tmplt.id, status_id=1, send_date=send_date.strftime('%-m/%-d/%-y'), queue_date=now, subject='', mesg='', send_offset=0, sent=0)
                        DBSession.add(emlqueue)
                Object.est = kw.get('cost_est')
                Object.ship_pkup = kw.get('ship_pkup')[0]
                Object.prod_apprv = kw.get('prod_apprv')
                Object.inputs = kw.get('input_stat')
                Object.ship_type_id = kw.get('ship_type')[0]
                Object.ship_notes = kw.get('ship_notes')
                #Object.prod_res = kw.get('prod_res')
                if kw.get('proj_id'):
                    Object.proj_id = kw.get('proj_id')
                Object.change_dtim = strftime('%-m/%-d')

            elif obj == 'Customer':
                Object = DBSession.query(Customer).get(kw.get('id'))
                val = kw.get('cust_name')
                val = val.replace('"',"")
                val = val.replace("\'","")
                val = val.replace("/","")
                val = val.replace("\#","")
                val = val.replace("[","")
                val = val.replace("]","")
                val = val.replace("{","")
                val = val.replace("}","")
                Object.name = val
                Object.ae_num = kw.get('ae_num')
                Object.cur_job_num = kw.get('cur_job_num')
                Object.contact = kw.get('contact')
                Object.email = kw.get('email')
                Object.phone = kw.get('phone')
                Object.notes = kw.get('notes')

            elif obj == 'Address':
                Object = DBSession.query(Address).get(kw.get('id'))
                Object.cust_id = kw.get('cust_id')
                Object.desc = kw.get('desc')
                Object.street1 = kw.get('street1')
                Object.street2 = kw.get('street2')
                Object.city = kw.get('city')
                Object.state = kw.get('state')
                Object.province = kw.get('province')
                Object.country = kw.get('country')
                Object.zipcode = kw.get('zipcode')

            elif obj == 'Project':
                Object = DBSession.query(Project).get(kw.get('id'))
                Object.status_id = kw.get('status_id')
                Object.rep_id = kw.get('rep_id')
                Object.proj_due = kw.get('proj_due')
                Object.name = kw.get('name')
                Object.notes = kw.get('notes')
                dtim_str = time.time.strftime('%-m/%-d')
                Object.create_dtim = kw.get(dtim_str)

            elif obj == 'MarkerRequest':
                Object = DBSession.query(MarkerRequest).get(kw.get('mkr_id'))
                Object.status_id = kw.get('mkr_status')
                Object.artist_id = kw.get('mkr_artist')
                #Object.proof.proof_path = kw.get('proof')
                Object.shirts = kw.get('mkr_shirts')
                Object.colors = kw.get('mkr_colors')
                Object.quantity = kw.get('mkr_qty')
                Object.post_to_web = kw.get('mkr_post')
                Object.copy = kw.get('copy')
                Object.notes = kw.get('notes')
                Object.idea = kw.get('idea')
                Object.new = kw.get('mkr_new_rev')
                Object.loc_1 = kw.get('loc_1')
                Object.loc_1_color = kw.get('color_1')
                Object.loc_2 = kw.get('loc_2')
                Object.loc_2_color = kw.get('color_2')
                Object.loc_3 = kw.get('loc_3')
                Object.loc_3_color = kw.get('color_3')
                Object.dsgn_time = kw.get('dsgn_time')
                Object.dsgn_cost = kw.get('dsgn_cost')
                Object.rev_notes = kw.get('rev_notes')
                Object.change_dtim = strftime('%-m/%-d')

            elif obj == 'Approval':
                Object = DBSession.query(Order).get(kw.get('id'))
                if Object.int_apprv_schd != kw.get('schd') and kw.get('schd') != None:
                    Object.int_apprv_schd = kw.get('schd')
                    Object.int_apprv_schd_date = strftime('%-m/%-d')
                if Object.int_apprv_ord != kw.get('ord') and kw.get('ord') != None:
                    Object.int_apprv_ord = kw.get('ord')
                    Object.int_apprv_ord_date = strftime('%-m/%-d')
                if Object.int_apprv_art != kw.get('art') and kw.get('art') != None:
                    Object.int_apprv_art = kw.get('art')
                    Object.int_apprv_art_date = strftime('%-m/%-d')
                if Object.int_apprv_cntd != kw.get('cntd') and kw.get('cntd') != None:
                    Object.int_apprv_cntd = kw.get('cntd')
                    Object.int_apprv_cntd_date = strftime('%-m/%-d')
                if Object.int_apprv_brnd != kw.get('brnd') and kw.get('brnd') != None:
                    Object.int_apprv_brnd = kw.get('brnd')
                    Object.int_apprv_brnd_date = strftime('%-m/%-d')
                if Object.int_apprv_invcd != kw.get('invcd') and kw.get('invcd') != None:
                    Object.int_apprv_invcd = kw.get('invcd')
                    Object.int_apprv_invcd_date = strftime('%-m/%-d')
                if Object.int_apprv_prdmgr != kw.get('prdmgr') and kw.get('prdmgr') != None:
                    Object.int_apprv_prdmgr = kw.get('prdmgr')
                    Object.int_apprv_prdmgr_date = strftime('%-m/%-d')
                if Object.int_apprv_prntr != kw.get('prntr') and kw.get('prntr') != None:
                    Object.int_apprv_prntr = kw.get('prntr')
                    Object.int_apprv_prntr_date = strftime('%-m/%-d')
                if Object.int_apprv_rep != kw.get('rep') and kw.get('rep') != None:
                    Object.int_apprv_rep = kw.get('rep')
                    Object.int_apprv_rep_date = strftime('%-m/%-d')
                if Object.int_apprv_chkd != kw.get('chkd') and kw.get('chkd') != None:
                    Object.int_apprv_chkd = kw.get('chkd')
                    Object.int_apprv_chkd_date = strftime('%-m/%-d')
                if Object.int_apprv_pakd != kw.get('pakd') and kw.get('pakd') != None:
                    Object.int_apprv_pakd = kw.get('pakd')
                    Object.int_apprv_pakd_date = strftime('%-m/%-d')
                if Object.int_apprv_shpd != kw.get('shpd') and kw.get('shpd') != None:
                    Object.int_apprv_shpd = kw.get('shpd')
                    Object.int_apprv_shpd_date = strftime('%-m/%-d')
                    Object.change_dtim = strftime('%-m/%-d')

            elif obj == 'PO':
                Object = DBSession.query(PO).get(kw.get('id'))
                Object.num = kw.get('num')
                Object.notes = kw.get('notes')
                Object.order_by = kw.get('order_by')
                Object.contact = kw.get('contact')
                Object.order_date = kw.get('order_date')
                Object.change_dtim = strftime('%-m/%-d') 

            elif obj == 'PODetail':
                Object = DBSession.query(PODetail).get(kw.get('id'))
                Object.vendor  = kw.get('vendor')
                Object.inbnd_date  = kw.get('inbnd_date')
                Object.ship_from  = kw.get('ship_from')
                Object.qty_misc  = kw.get('qty_misc')
                Object.qty_xs  = kw.get('qty_xs')
                Object.qty_sm  = kw.get('qty_sm')
                Object.qty_md  = kw.get('qty_md')
                Object.qty_lg  = kw.get('qty_lg')
                Object.qty_xl  = kw.get('qty_xl')
                Object.qty_2xl  = kw.get('qty_2xl')
                Object.qty_3xl  = kw.get('qty_3xl')
                Object.qty_4xl  = kw.get('qty_4xl')
                Object.hot  = kw.get('hot')
                Object.crit_need_by  = kw.get('crit_need_by')
                Object.rcvd_date  = kw.get('rcvd_date')

            elif obj == 'Worksheet':
                Object = DBSession.query(Worksheet).get(kw.get('id'))
                Object.notes = kw.get('notes')

            elif obj == 'WorksheetDetail':
                Object = DBSession.query(WorksheetDetail).get(kw.get('id'))
                Object.qty_misc  = kw.get('qty_misc')
                Object.qty_xs  = kw.get('qty_xs')
                Object.qty_sm  = kw.get('qty_sm')
                Object.qty_md  = kw.get('qty_md')
                Object.qty_lg  = kw.get('qty_lg')
                Object.qty_xl  = kw.get('qty_xl')
                Object.qty_2xl  = kw.get('qty_2xl')
                Object.qty_3xl  = kw.get('qty_3xl')
                Object.qty_4xl  = kw.get('qty_4xl')
                Object.hot  = kw.get('hot')

            elif obj == 'OrderOption':
                Object = DBSession.query(OrderOption).get(kw.get('id'))
                Object.price = kw.get('price')
                Object.qty = str(kw.get('qty'))
                Object.active = kw.get('active')
                Object.pushed = kw.get('pushed')

            elif obj == 'EmailQueue':
                Object = DBSession.query(EmailQueue).get(kw.get('id'))
                #Object.status_id = kw.get('status_id')
                Object.send_date = kw.get('send_date')
                Object.queue_date = kw.get('queue_date')
                Object.send_offset = kw.get('send_offset')
                Object.mesg = kw.get('custom_message')
                Object.subject = kw.get('custom_subject')
                if kw.get('emlstat') == 'q':
                    Object.status_id = 1
                elif kw.get('emlstat') == 'h':
                    Object.status_id = 2 
                id = kw.get('job_id')

            flash("Edit saved!", "success")

        except:
            log.debug(page + '    ' + obj + 'id = ' + kw.get('id'))
            flash("Error saving edit!", "danger")

        DBSession.flush()
		
        redirect("/" + page + "/?id=" + str(id))

    #--->

    #@expose('nodinxprd.templates.about')
    #def about(self):
    #    """Handle the 'about' page."""
    #    return dict(page='about')

    #--->

    #@expose('nodinxprd.templates.environ')
    #def environ(self):
    #    """This method showcases TG's access to the wsgi environment."""
    #    return dict(page='environ', environment=request.environ)

    #--->

    #@expose('nodinxprd.templates.data')
    #@expose('json')
    #def data(self, **kw):
        #"""
        #This method showcases how you can use the same controller
        #for a data page and a display page.
        #"""
        #return dict(page='data', params=kw)

    #--->

    @expose('json')
    @require(predicates.not_anonymous())
    def fetch_custs(self, **kw):
        """Handle a fetch of customers for auto-complete select"""

        options = DBSession.query(Customer).options(load_only("name")).filter(Customer.name.like('%' + kw.get('') + '%')).all()
 
        return dict(data=options)

    #--->

    #@expose('nodinxprd.templates.index')
    #@require(predicates.has_permission('manage', msg=l_('Only for managers')))
    #def manage_permission_only(self, **kw):
    #    """Illustrate how a page for managers only works."""
    #    return dict(page='managers stuff')

    #--->

    #@expose('nodinxprd.templates.index')
    #@require(predicates.is_user('editor', msg=l_('Only for the editor')))
    #def editor_user_only(self, **kw):
    #    """Illustrate how a page exclusive for the editor works."""
    #    return dict(page='editor stuff')

    #--->

    @expose('nodinxprd.templates.login')
    def login(self, came_from=lurl('/'), failure=None, login=''):
        """Start the user login."""
        if failure is not None:
            if failure == 'user-not-found':
                flash(_('User not found'), 'danger')
            elif failure == 'invalid-password':
                flash(_('Invalid Password'), 'danger')

        login_counter = request.environ.get('repoze.who.logins', 0)
        if failure is None and login_counter > 0:
            flash(_('Wrong credentials'), 'warning')

        return dict(page='login', login_counter=str(login_counter),
                    came_from=came_from, login=login)

    #--->

    @expose()
    @require(predicates.not_anonymous())
    def post_login(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on successful
        authentication or redirect her back to the login page if login failed.

        """
        if not request.identity:
            login_counter = request.environ.get('repoze.who.logins', 0) + 1
            redirect('/login',
                     params=dict(came_from=came_from, __logins=login_counter))
        userid = request.identity['repoze.who.userid']
        flash(_('Welcome back, %s!') % userid, 'info')

        # Do not use tg.redirect with tg.url as it will add the mountpoint
        # of the application twice.
        return HTTPFound(location=came_from)

    #--->

    @expose()
    def post_logout(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on logout and say
        goodbye as well.

        """
        flash(_('Goodbye'))
        return HTTPFound(location=came_from)

__all__ = ['RootController', 'DeclarativeOrderController', 'DeclarativeCustomerController', 'DeclarativeVendorController', 'DeclarativeMarkerRequestController', 'DeclarativeShipTypeController', 'DeclarativeOrderTypeController', 'DeclarativeCategoryController', 'DeclarativeOrderStatusController', 'DeclarativePOController', 'DeclarativePODetailController', 'DeclarativeOrderDetailController', 'DeclarativeProofController']
