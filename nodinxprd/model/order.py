# -*- coding: utf-8 -*-
"""Orders model module."""
from sqlalchemy import *
from sqlalchemy import Table, ForeignKey, Column
from sqlalchemy.types import Integer, Unicode, DateTime, Date, Boolean, LargeBinary, Numeric
from sqlalchemy.orm import relationship, backref

from nodinxprd.model import DeclarativeBase, metadata, DBSession

class Contact(DeclarativeBase):
    __tablename__ = 'contacts'

    id = Column(Integer, autoincrement=True, primary_key=True)
    cust_id = Column(Integer, ForeignKey('customers.id'), index=True)
    type = Column(Text)
    name = Column(Text)
    phone = Column(Text)
    email = Column(Text)
    notes = Column(Text)

    def repr(self):
        return("Contact")

class NewRev(DeclarativeBase):
    __tablename__ = 'newrev'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(255))

    def repr(self):
        return("NewRev")

class ShipPkup(DeclarativeBase):
    __tablename__ = 'shippkup'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(255))

    def repr(self):
        return("ShipPkup")

class YesNo(DeclarativeBase):
    __tablename__ = 'yesno'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(255))

    def repr(self):
        return("YesNo")

class Proof(DeclarativeBase):
    __tablename__ = 'proofs'

    id = Column(Integer, autoincrement=True, primary_key=True)
    proof_path = Column(Text)
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    marker_id = Column(Integer, ForeignKey('mrkrequest.id'), index=True)

    def repr(self):
        return("Proof")

class Role(DeclarativeBase):
    __tablename__ = 'roles'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(32))
    perms = Column(Unicode(32))
    
    def repr(self):
        return("Role")

class Priority(DeclarativeBase):
    __tablename__ = 'priorities'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(64))

    def repr(self):
        return("Priority")

class Resource(DeclarativeBase):
    __tablename__ = 'resourcetypes'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(64))

    def repr(self):
        return("Resource")

class PrintTime(DeclarativeBase):
    __tablename__ = 'printtimes'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(64))

    def repr(self):
        return("PrintTime")

class InbndInvDesc(DeclarativeBase):
    __tablename__ = 'inbndinvdescs'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(64))

    def repr(self):
        return("InbndInvDesc")

class OrderStatus(DeclarativeBase):
    __tablename__ = 'orderstats'

    id = Column(Integer, autoincrement=True, primary_key=True)
    status = Column(Unicode(255))
    bg_color = Column(Unicode(32))

    def repr(self):
        return("OrderStatus")

class OrderType(DeclarativeBase):
    __tablename__ = 'ordertypes'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(255))

    def repr(self):
        return("OrderType")

class ItemType(DeclarativeBase):
    __tablename__ = 'itemtypes'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(255))
    ord_type_id = Column(Integer)

    def repr(self):
        return("ItemType")

class Item(DeclarativeBase):
    __tablename__ = 'items'

    id = Column(Integer, autoincrement=True, primary_key=True)
    v_item = Column(Unicode(128), default=u'')
    color = Column(Unicode(64), default=u'')
    desc = Column(Unicode(255), default=u'')
    type = Column(Integer, ForeignKey('itemtypes.id'), index=True)
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    itemtype = relationship('ItemType')

    def repr(self):
        return("Item")

class Category(DeclarativeBase):
    __tablename__ = 'categories'

    id = Column(Integer, autoincrement=True, primary_key=True)
    desc = Column(Unicode(255))

    def repr(self):
        return("Category")

class MarkerStatus(DeclarativeBase):
    __tablename__ = 'markerstatuses'

    id = Column(Integer, autoincrement=True, primary_key=True)
    mrk_status = Column(Unicode(64))

    def repr(self):
        return("MarkerStatus")

class ShipType(DeclarativeBase):
    __tablename__ = 'shiptypes'

    id = Column(Integer, autoincrement=True, primary_key=True)
    type = Column(Unicode(255))

    def repr(self):
        return("ShipType")

class Rep(DeclarativeBase):
    __tablename__ = 'reps'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(255))
    user_id = Column(Integer, ForeignKey('tg_user.user_id'), index=True)
    user = relationship('User')

    def repr(self):
        return("Rep")

class Artist(DeclarativeBase):
    __tablename__ = 'artists'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(255))
    user_id = Column(Integer, ForeignKey('tg_user.user_id'), index=True)
    user = relationship('User')

    def repr(self):
        return("Artist")

class Approve(DeclarativeBase):
    __tablename__ = 'approves'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(255))

    def repr(self):
        return("Approve")

class Customer(DeclarativeBase):
    __tablename__ = 'customers'

    id = Column(Integer, autoincrement=True, primary_key=True)
    ae_num = Column(Unicode(32))
    notes = Column(Unicode(255))
    cur_job_num = Column(Unicode(32), index=True)
    name = Column(Unicode(255))
    contact = Column(Unicode(255))
    email = Column(Unicode(128))
    phone = Column(Unicode(32))
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    last_mkt_msg = Column(Text)

    def repr(self):
        return("Customer")

class Vendor(DeclarativeBase):
    __tablename__ = 'vendors'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(255))
    terms = Column(Unicode(255))
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 

    def repr(self):
        return("Vendor")

class Address(DeclarativeBase):
    __tablename__ = 'addresses'

    id = Column(Integer, autoincrement=True, primary_key=True)
    desc = Column(Unicode(256))
    cust_id = Column(Integer, ForeignKey('customers.id'), index=True)
    street1 = Column(Unicode(256))
    street2 = Column(Unicode(256))
    city = Column(Unicode(256))
    state = Column(Unicode(256))
    province = Column(Unicode(256))
    country = Column(Unicode(256))
    zipcode = Column(Unicode(32))

    def repr(self):
        return("Address")

class Order(DeclarativeBase):
    __tablename__ = 'orders'

    id = Column(Integer, autoincrement=True, primary_key=True)
    job_name = Column(Unicode(255))
    job_num = Column(Unicode(32), index=True)
    cust_id = Column(Integer, ForeignKey('customers.id'), index=True)
    order_date = Column(Unicode(64))
    proof_date = Column(Unicode(64))
    due_date = Column(Unicode(64))
    ord_type_id = Column(Integer, ForeignKey('ordertypes.id'), index=True) 
    prod_apprv = Column(Integer)
    est = Column(Unicode(64))
    thnks_sent = Column(Integer)
    ship_pkup = Column(Integer, ForeignKey('shippkup.id'))
    inputs = Column(Unicode(64))   
    ship_type_id = Column(Integer, ForeignKey('shiptypes.id'))
    ord_stat_id = Column(Integer, ForeignKey('orderstats.id'), index=True)
    rep_id = Column(Integer, ForeignKey('reps.id'), index=True)
    rush = Column(Integer, ForeignKey('printtimes.id'))
    hold = Column(Integer)
    cust_apprv = Column(Integer)
    prod_notes = Column(Text)
    proc_date = Column(Unicode(64))
    int_apprv_schd = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_schd_date = Column(Text)
    int_apprv_ord = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_ord_date = Column(Text)
    int_apprv_art = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_art_date = Column(Text)
    int_apprv_cntd = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_cntd_date = Column(Text)
    int_apprv_brnd = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_brnd_date = Column(Text)
    int_apprv_invcd = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_invcd_date = Column(Text)
    int_apprv_prdmgr = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_prdmgr_date = Column(Text)
    int_apprv_prntr = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_prntr_date = Column(Text)
    int_apprv_rep = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_rep_date = Column(Text)
    int_apprv_chkd = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_chkd_date = Column(Text)
    int_apprv_pakd = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_pakd_date = Column(Text)
    int_apprv_shpd = Column(Unicode(1024), default=u'glyphicon glyphicon-remove')
    int_apprv_shpd_date = Column(Text)
    proj_id = Column(Integer, ForeignKey('projects.id'), index=True)
    prod_res = Column(Integer, ForeignKey('resourcetypes.id'), index=True)
    archived = Column(Boolean, default=False)
    charity = Column(Boolean, default=False)
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    marker_id = Column(Integer, ForeignKey('mrkrequest.id'), index=True)
    numsheet = Column(Text)
    ship_notes = Column(Text)
    OrderType = relationship('OrderType')
    Customer = relationship('Customer')
    OrderStatus = relationship('OrderStatus')
    PrintTime = relationship('PrintTime')
    Rep = relationship('Rep')
    Resource = relationship('Resource')

    def repr(self):
        return("Order")

class OrderDetail(DeclarativeBase):
    __tablename__ = 'orderdetails'

    id = Column(Integer, autoincrement=True, primary_key=True)
    pushed = Column(Boolean, default=False)
    ord_id = Column(Integer, ForeignKey('orders.id'), index=True)
    item_id = Column(Integer, ForeignKey('items.id'), index=True)
    qty_xs = Column(Text, default='')
    cnt_xs = Column(Unicode(32), default=u'')
    prnt_xs = Column(Unicode(32), default=u'')
    qty_sm = Column(Text, default='')
    cnt_sm = Column(Unicode(32), default=u'')
    prnt_sm = Column(Unicode(32), default=u'')
    qty_md = Column(Text, default='')
    cnt_md = Column(Unicode(32), default=u'')
    prnt_md = Column(Unicode(32), default=u'')
    qty_lg = Column(Text, default='')
    cnt_lg = Column(Unicode(32), default=u'')
    prnt_lg = Column(Unicode(32), default=u'')
    qty_xl = Column(Text, default='')
    cnt_xl = Column(Unicode(32), default=u'')
    prnt_xl = Column(Unicode(32), default=u'')
    qty_2xl = Column(Text, default='')
    cnt_2xl = Column(Unicode(32), default=u'')
    prnt_2xl = Column(Unicode(32), default=u'')
    qty_3xl = Column(Text, default='')
    cnt_3xl = Column(Unicode(32), default=u'')
    prnt_3xl = Column(Unicode(32), default=u'')
    qty_4xl = Column(Text, default='')
    cnt_4xl = Column(Unicode(32), default=u'')
    prnt_4xl = Column(Unicode(32), default=u'')
    qty_misc = Column(Text, default='')
    cnt_misc = Column(Unicode(32), default=u'')
    prnt_misc = Column(Unicode(32), default=u'')
    price = Column(Text, default='0.00')
    l1 = Column(Unicode(1), default=u'')
    l1_so = Column(Unicode(64), default=u'')
    l2 = Column(Unicode(1), default=u'')
    l2_so = Column(Unicode(64), default=u'')
    l3 = Column(Unicode(1), default=u'')
    l3_so = Column(Unicode(64), default=u'')
    l4 = Column(Unicode(1), default=u'')
    l4_so = Column(Unicode(64), default=u'')
    l5 = Column(Unicode(1), default=u'')
    l5_so = Column(Unicode(64), default=u'')
    l6 = Column(Unicode(1), default=u'')
    l6_so = Column(Unicode(64), default=u'')
    on_order = Column(Boolean, default=False)
    dnotes = Column(Text)
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    order = relationship("Order", back_populates="details")
    item = relationship("Item")

    def repr(self):
        return("OrderDetail")

Order.details = relationship('OrderDetail', back_populates='order')

class PO(DeclarativeBase):
    __tablename__ = 'po'

    id = Column(Integer, autoincrement=True, primary_key=True)
    num = Column(Unicode(256))
    notes = Column(Unicode(255))
    contact = Column(Unicode(255))
    order_date = Column(Unicode(64))
    order_by = Column(Integer, ForeignKey('tg_user.user_id'), index=True)
    closed = Column(Boolean, default=0)
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 

    def repr(self):
        return("PO")

class PODetail(DeclarativeBase):
    __tablename__ = 'podetails'

    id = Column(Integer, autoincrement=True, primary_key=True)
    po_id = Column(Integer, ForeignKey('po.id'), index=True)
    ordered = Column(Boolean, default=False)
    item = Column(Text)
    color = Column(Text)
    desc = Column(Text)
    inbnd_date = Column(Unicode(64))
    rcvd_date = Column(Unicode(64))
    vendor = Column(Unicode(128), default=u'')
    ship_from = Column(Unicode(32), default=u'')
    qty_xs = Column(Text, default='')
    qty_sm = Column(Text, default='')
    qty_md = Column(Text, default='')
    qty_lg = Column(Text, default='')
    qty_xl = Column(Text, default='')
    qty_2xl = Column(Text, default='')
    qty_3xl = Column(Text, default='')
    qty_4xl = Column(Text, default='')
    qty_misc = Column(Text, default='')
    wk_dets = Column(Text, default='')
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    po = relationship('PO', back_populates='details')

    def repr(self):
        return("PODetail")

PO.details = relationship('PODetail', order_by=PODetail.id, back_populates='po')

class Worksheet(DeclarativeBase):
    __tablename__ = 'worksheets'

    id = Column(Integer, autoincrement=True, primary_key=True)
    po_id = Column(Integer, ForeignKey('po.id'), index=True)
    desc = Column(Unicode(256))
    frozen = Column(Boolean, default=False)
    notes = Column(Text)
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 

    def repr(self):
        return("Worksheet")

class WorksheetDetail(DeclarativeBase):
    __tablename__ = 'wkshdetails'

    id = Column(Integer, autoincrement=True, primary_key=True)
    wk_id = Column(Integer, ForeignKey('worksheets.id'), index=True)
    item_id = Column(Integer, ForeignKey('items.id'), index=True)
    hot = Column(Boolean, default=False)
    ordd_id = Column(Integer, ForeignKey('orderdetails.id'), index=True)
    allocated = Column(Boolean, default=False)
    crit_need_by = Column(Unicode(64))
    qty_xs = Column(Text, default='')
    qty_sm = Column(Text, default='')
    qty_md = Column(Text, default='')
    qty_lg = Column(Text, default='')
    qty_xl = Column(Text, default='')
    qty_2xl = Column(Text, default='')
    qty_3xl = Column(Text, default='')
    qty_4xl = Column(Text, default='')
    qty_misc = Column(Text, default='')
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    orddet = relationship('OrderDetail', backref='wkshdetails')
    item = relationship('Item')

    def repr(self):
        return("WorksheetDetail")

Worksheet.details = relationship('WorksheetDetail', order_by='WorksheetDetail.id')

class MarkerRequest(DeclarativeBase):
    __tablename__ = 'mrkrequest'

    id = Column(Integer, autoincrement=True, primary_key=True)
    status_id = Column(Integer, ForeignKey('markerstatuses.id'), index=True)
    artist_id = Column(Integer, ForeignKey('artists.id'))
    ord_id = Column(Integer)
    proof_id = Column(Integer)
    shirts = Column(Unicode(255))
    colors = Column(Unicode(255))
    quantity = Column(Unicode(128))
    rev_notes = Column(Text)
    copy = Column(Unicode(2048))
    idea = Column(Unicode(2048))
    notes = Column(Unicode(2048))
    new = Column(Integer, default=1)
    loc_1 = Column(Unicode(255))
    loc_1_color = Column(Unicode(255))
    loc_2 = Column(Unicode(255))
    loc_2_color = Column(Unicode(255))
    loc_3 = Column(Unicode(255))
    loc_3_color = Column(Unicode(255))
    dsgn_time = Column(Text, default=0)
    rev_num = Column(Integer, default=1)
    cnt = Column(Integer, default=1)
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    MarkerStatus = relationship('MarkerStatus')
    artist = relationship('Artist')
    proofs = relationship("Proof")

    def repr(self):
        return("MarkerRequest")

Order.marker = relationship('MarkerRequest')

class MarkerRequestLog(DeclarativeBase):
    __tablename__ = 'mrkrqlog'

    id = Column(Integer, autoincrement=True, primary_key=True)
    ord_id = Column(Integer, ForeignKey('orders.id'), index=True)
    marker_id = Column(Integer, ForeignKey('mrkrequest.id'), index=True)
    date_in = Column(Text)
    new = Column(Integer, default=0)
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    MarkerRequest = relationship('MarkerRequest')
    order = relationship('Order')

    def repr(self):
        return("MarkerRequestLog")

class ProjectStat(DeclarativeBase):
    __tablename__ = 'projectstats'

    id = Column(Integer, autoincrement=True, primary_key=True)
    proj_stat = Column(Unicode(255))
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 

    def repr(self):
        return("ProjectStat")

class Project(DeclarativeBase):
    __tablename__ = 'projects'

    id = Column(Integer, autoincrement=True, primary_key=True)
    status_id = Column(Integer, ForeignKey('projectstats.id'), index=True)
    rep_id = Column(Integer, ForeignKey('reps.id'), index=True)
    proj_due = Column(Unicode(64))
    name = Column(Unicode(256))
    notes = Column(Unicode(4096))
    create_dtim = Column(Text)
    change_dtim = Column(Text)
    change_user = Column(Unicode(64)) 
    jobs = relationship('Order', backref=('projects'))
    prj_stat = relationship('ProjectStat')
    reps = relationship('Rep')

    def repr(self):
        return("Project")

class OptionType(DeclarativeBase):
    __tablename__ = 'optiontypes'

    id = Column(Integer, autoincrement=True, primary_key=True)
    desc = Column(Unicode(255), default=u'')
    sdesc = Column(Unicode(32), default=u'')
    price = Column(Unicode(32), default=u'0')
    ord_type_id = Column(Integer, ForeignKey('ordertypes.id'), index=True) 
    catg_id = Column(Integer, ForeignKey('categories.id'), index=True)
    category = relationship('Category')
    order_type = relationship('OrderType')

    def repr(self):
        return("OptionTypes")

class OrderOption(DeclarativeBase):
    __tablename__ = 'orderoptions'

    id = Column(Integer, autoincrement=True, primary_key=True)
    ordd_id = Column(Integer, ForeignKey('orderdetails.id'))
    opt_id = Column(Integer, ForeignKey('optiontypes.id'))
    price = Column(Unicode(32), default=u'0.00')
    qty = Column(Text, default='')
    active = Column(Boolean, default=0)
    pushed = Column(Boolean, default=0)
    desc = Column(Text)
    orddet = relationship("OrderDetail", back_populates="options")
    type = relationship("OptionType")

    def repr(self):
        return("OrderOption")

OrderDetail.options = relationship('OrderOption')

class ItemOption(DeclarativeBase):
    __tablename__ = 'itemoptions'

    id = Column(Integer, autoincrement=True, primary_key=True)
    item_type_id = Column(Integer, ForeignKey('itemtypes.id'), index=True)
    opt_type_id = Column(Integer, ForeignKey('optiontypes.id'), index=True)

    def repr(self):
        return("ItemOption")

class Unit(DeclarativeBase):
    __tablename__ = 'units'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(128))
    desc = Column(Unicode(1048))

    def repr(self):
        return("Unit")

class EmailTemplate(DeclarativeBase):
    __tablename__ = 'emailtemplates'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(128))
    desc = Column(Unicode(1048))
    value = Column(Text)
    subject = Column(Text)
    message = Column(Text)
    img = Column(Text)
    offset = Column(Integer)
    active = Column(Boolean)

    def repr(self):
        return("EmailTemplate")

class EmailStatus(DeclarativeBase):
    __tablename__ = 'emailstatus'

    id = Column(Integer, autoincrement=True, primary_key=True)
    status = Column(Unicode(255))
    bg_color = Column(Unicode(32))

    def repr(self):
        return("EmailStatus")

class EmailQueue(DeclarativeBase):
    __tablename__ = 'emailqueue'

    id = Column(Integer, autoincrement=True, primary_key=True)
    ord_id = Column(Integer, ForeignKey('orders.id'), index=True)
    tmplt_id = Column(Integer, ForeignKey('emailtemplates.id'), index=True)
    status_id = Column(Integer, ForeignKey('emailstatus.id'), index=True)
    send_date = Column(Text)
    queue_date = Column(Text)
    mesg = Column(Text)
    send_offset = Column(Text)
    recips = Column(Text)
    subject = Column(Text)
    sent = Column(Boolean, default=False)
    emailstatus = relationship('EmailStatus')
    emailtemplate = relationship('EmailTemplate')

    def repr(self):
        return("EmailQueue")

class EmailLog(DeclarativeBase):
    __tablename__ = 'emaillog'

    id = Column(Integer, autoincrement=True, primary_key=True)
    ord_id = Column(Integer, ForeignKey('orders.id'), index=True)
    tmplt_id = Column(Integer, ForeignKey('emailtemplates.id'), index=True)
    sender_id = Column(Text)
    sent_date = Column(Text)
    recips = Column(Text)
    subject = Column(Text)
    mesg = Column(Text)

    def repr(self):
        return("EmailLog")

class Config(DeclarativeBase):
    __tablename__ = 'configs'

    id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(128))
    desc = Column(Unicode(1048))
    value = Column(Text)

    def repr(self):
        return("Config")

__all__ = ['YesNo', 'ShipPkup', 'OrderStatus', 'OrderType', 'ShipType', 'Order', 'PO', 'PODetail', 'Customer', 'Vendor', 'MarkerRequest', 'MarkerRequestLog', 'Proof', 'OrderDetail', 'OptionType', 'OrderOption', 'Category', 'Item', 'Priority', 'MarkerStatus', 'InbndInvDesc', 'PrintTime', 'ProjectStat', 'Project', 'Address', 'Role', 'Config', 'NewRev', 'Artist', 'Resource', 'ItemType', 'ItemOption', 'Unit', 'Worksheet', 'WorksheetDetail', 'Contact', 'EmailLog', 'EmailStatus', 'EmailQueue', 'EmailTemplate']
