// DATATABLES INIT FOR JOBGRID IN DASHBOARD.XHTML

$(document).ready(function() {
	$('.grid').addClass('hover');
	var jobGrid = $('#jobGrid .grid').DataTable({
		paging: false,
		info: false,
		colReorder: false,
		rowReorder: false
	});
	new $.fn.dataTable.FixedHeader( jobGrid, {
		headerOffset: $('.navbar').outerHeight()
	});
	new $.fn.dataTable.Buttons( jobGrid, {
		buttons: ['copy', 'pdf', 'csv',
			{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LETTER'
            }] 
	});
	
	jobGrid.buttons().container().appendTo( $('.col-sm-6:eq(0)', jobGrid.table().container() ) );
	
	$('#jobGrid .grid tbody').on( 'mouseenter', 'td', function () {
		var colIdx = jobGrid.cell(this).index().column;
		$( jobGrid.cells().nodes() ).removeClass( 'highlight' );
		$( jobGrid.column( colIdx ).nodes() ).addClass( 'highlight' );
	});

});
