// DATATABLES INIT FOR EDITOR - USES CLASS ".dt-editor"

var editor;
	 
$(document).ready(function(){
	editor = new $.fn.dataTable.Editor({
		ajax: "../php/staff.php",
		table: ".table-responsive.dt-editor .grid",
		fields: [ 
			{label: "Item", name: "v_item"},
			{label: "Color", name: "color"},
			{label: "Desc", name: "desc"}
		]
	});
 
	$('.table-responsive.dt-editor .grid').DataTable( {
		dom: "Bfrtip",
		ajax: "../php/staff.php",
		columns: [
			{ data: null, render: function ( data, type, row ) {
				// Combine the first and last names into a single table field
				return data.first_name+' '+data.last_name;
			} },
			{ data: "position" },
			{ data: "office" },
			{ data: "extn" },
			{ data: "start_date" },
			{ data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
		],
		select: true,
		buttons: [
			{ extend: "create", editor: editor },
			{ extend: "edit",   editor: editor },
			{ extend: "remove", editor: editor }
		]
	} );
} );

