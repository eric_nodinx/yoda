// DATATABLES INIT FOR JOBGRID IN DASHBOARD.XHTML

$(document).ready(function() {
	$('.grid').addClass('hover');
	var detGrid = $('#job-details .grid').DataTable({
		paging: false,
		info: false,
		colReorder: false,
		rowReorder: false
	});
	new $.fn.dataTable.FixedHeader( detGrid, {
		headerOffset: $('.navbar').outerHeight()
	});
	//new $.fn.dataTable.Buttons( detGrid, {
	//	buttons: ['copy', 'pdf', 'csv',
		//	{
         //      extend: 'pdfHtml5',
         //      orientation: 'landscape',
         //      pageSize: 'LETTER'
         //  }] 
	//});
	
	//detGrid.buttons().container().appendTo( $('.col-sm-6:eq(0)', detGrid.table().container() ) );
	
	$('#detGrid .grid tbody').on( 'mouseenter', 'td', function () {
		var colIdx = detGrid.cell(this).index().column;
		$( detGrid.cells().nodes() ).removeClass( 'highlight' );
		$( detGrid.column( colIdx ).nodes() ).addClass( 'highlight' );
	});

});
