// DATATABLES INIT FOR ANYTHING - ADD CLASS "dt-basic"

$(document).ready(function(){
	var dtBasic = $('.table-responsive.dt-basic .grid').DataTable({
		bFilter: false,
		paging: false,
		info: false,
		autoFill: false,
		select: false,
		responsive: false,
		keys: false
	});

	new $.fn.dataTable.Buttons( dtBasic, {
		buttons: ['copy', 'pdf', 'csv'] 
	});

	dtBasic.buttons().container().appendTo( $('.col-sm-12:eq(0)', dtBasic.table().container() ) );

});
