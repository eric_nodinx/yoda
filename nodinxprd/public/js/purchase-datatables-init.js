// DATATABLES INIT FOR PRCHGRID IN PURCHASE.XHTML

$(document).ready(function() {
	$('.grid').addClass('hover');
	var prchGrid = $('#prchGrid .grid').DataTable({
		paging: false,
		info: false,
		colReorder: false,
		rowReorder: true
	});
	new $.fn.dataTable.FixedHeader( prchGrid, {
		headerOffset: $('.navbar').outerHeight()
	});
	new $.fn.dataTable.Buttons( prchGrid, {
		buttons: ['copy', 'pdf', 'csv',
			{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LETTER'
            }] 
	});
	
	prchGrid.buttons().container().appendTo( $('.col-sm-6:eq(0)', prchGrid.table().container() ) );
	
	$('#prchGrid .grid tbody').on( 'mouseenter', 'td', function () {
		var colIdx = prchGrid.cell(this).index().column;
		$( prchGrid.cells().nodes() ).removeClass( 'highlight' );
		$( prchGrid.column( colIdx ).nodes() ).addClass( 'highlight' );
	});

});
