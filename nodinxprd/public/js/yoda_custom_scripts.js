$(document).ready(function() {

	// IF TOUCH
	if ($('html').hasClass('touch')) {
		$('input[ontouchstart]').attr('disabled', 'disabled');
		$('.visible-touch').show();
		$('.hidden-touch').hide();
	} else if ($('html').hasClass('no-touch')) {
		$('.visible-notouch').show();
		$('.hidden-notouch').hide();
	}

	$('input[ontouchstart]').each(function() {

	   var timeout, longtouch;

	   $(this).bind('touchstart', function() {
	      timeout = setTimeout(function() {
	          longtouch = true;
	          $(this).css({'background-color':"#ff00ff"});
	      }, 1000);
	   }).bind('touchend', function() {
	       if (longtouch) {
	          $(this).css({'background-color':"#00ff00"}).val('ER');
	       }
	       longtouch = false;
	       clearTimeout(timeout);
	   });

	});

	// GRACEFULLY SHOW DT BUTTONS AND FILTER
	var rH = $('#DataTables_Table_0_wrapper').children('.row:eq(0)').outerHeight();

	$('#DataTables_Table_0_wrapper').children('.row:eq(0)').each(function(){
		//hide row and children on load
		$(this).css({height: 0, overflow: "hidden"});
		$(this).find('.dt-buttons').css({opacity: 0});
		$(this).find('.dataTables_filter').css({opacity: 0});
	});

	function showDTfilter(rH){
		setTimeout(function(){
			//animate visibility of row
			$('#DataTables_Table_0_wrapper').children('.row:eq(0)').animate({height: "+=" + rH}, 1000, function(){
				$(this).find('.dt-buttons').animate({opacity: 1}, 500, function(){});
				$(this).find('.dataTables_filter').animate({opacity: 1}, 500, function(){});
			});
		}, 1000, function(){});
	}
	showDTfilter(rH);
	

	// HIGHLIGHT TABLE ROWS
	$('.highlight-rows').find('.grid').addClass('hover');


	// START AUTO SUBMIT AFTER FILE PICK
	function submitMarker(inputID,formID){
		var fID = "#" + formID;
		setTimeout(function(){
			$(fID).submit();
		}, 500);
	}
	$('input[type=file]').on('change',function(){
		var xx = $(this).attr('id'), yy = $(this).parents('form').attr('id');
		submitMarker(xx,yy);
	});
	// END AUTO SUBMIT AFTER FILE PICK


	// GRACEFULLY SHOW ALERTS
	function showAlert(){
		setTimeout(function(){
			//show alert
			$('.alert').animate({top: "+=60"}, 1000, function(){
				//callback
				$('body').removeClass("alert-hidden").addClass("alert-visible");
			});
		}, 1000);
	}


	// GRACEFULLY HIDE ALERTS
	function hideAlert(){
		setTimeout(function(){
			//show alert
			$('.alert').animate({top: "-=60"}, 1000, function(){
				//callback
				$('body').animate({paddingTop: "-=0"}, 1000, function(){
					//callback
					$('body').removeClass("alert-visible").addClass("alert-hidden");
				});
			});
		}, 0);
	}
	

	// SHOW ALERT
	if ( $('body .alert').length ){
		$('body').has('.alert').addClass('has-alert');
		showAlert();
	};
	

	// CLOSE ALERT
	$('.alert .close').on('click',function(){
		hideAlert();
	});


	// GRACEFULLY SHOW AND WIDEN TINYMCE EDITOR
	setTimeout(function(){
		$('table.mceLayout').css({"width":"100%"});
		$('table.mceLayout').animate({opacity: 1}, 1000);
	}, 1000);


	// START LIVE SEARCH FUNCTION
	$(".search").keyup(function () {
		var searchTerm = $(".search").val();
		var listItem = $('.grid tbody').children('tr');
		var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
		$.extend($.expr[':'], {'containsi': function(elem, i, match, array){return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0; } } );
		$(".grid tbody trs").not(":containsi('" + searchSplit + "')").each(function(e){$(this).attr('visible','false'); });
		$(".grid tbody tr:containsi('" + searchSplit + "')").each(function(e){$(this).attr('visible','true'); });
		var jobCount = $('.grid tbody tr[visible="true"]').length;
		$('.counter').text(jobCount + ' item(s)');
		if( jobCount == '0' ) { $('.no-result').show(); } else { $('.no-result').hide(); }
	});
	// END LIVE SEARCH FUNCTION


	// START ORDER DETAIL COUNT STATUS
		var	countStatus = $('.counting-toggle .count-status').attr('countstatus'),
		statusIcon = $('.counting-toggle .count-status .glyphicon');

		// set or get current value
		countStatus = 0;

		// check increment value for css class assignments
		countToggle();

		// set attr for css icons
		//$('.counting-toggle .count-status').attr('countstatus', countStatus);

		// situational awareness
		//console.log("countStatus = " + countStatus);

		$(".counting-toggle").click(function(){
			// increment
			countStatus++;

			// reset check
			//if (countStatus == "3"){countStatus = 0;}

			// check increment value for css class assignments
			countToggle();

			// update attr for css icons
			//$('.counting-toggle .count-status').attr('countstatus', countStatus);

			// situational awareness
			//console.log("countStatus = " + countStatus);
		});

		function countToggle(){
			if ( countStatus == "3" ) {countStatus = 0;}
			if ( countStatus == "0" ) {$(statusIcon).addClass("glyphicon-alert").removeClass("glyphicon-ok").removeClass("glyphicon-remove");}
			if ( countStatus == "1" ) {$(statusIcon).addClass("glyphicon-ok").removeClass("glyphicon-alert").removeClass("glyphicon-remove");}
			if ( countStatus == "2" ) {$(statusIcon).addClass("glyphicon-remove").removeClass("glyphicon-ok").removeClass("glyphicon-alert");}

			// update attr for css icons
			$('.counting-toggle .count-status').attr('countstatus', countStatus);

			// situational awareness
			//console.log("countStatus = " + countStatus);
		}
	// END ORDER DETAIL COUNT STATUS

	// START ACCORDION
		$( ".accordion" ).accordion({
			heightStyle: "content",
			header: ".panel-heading",
			active: 0,
			collapsible: false
		});
	// END ACCORDION

	// START CHOSEN
		$(".chosen-init").chosen({
			width: "100%"
		});
	// END CHOSEN

	// START MULTISELECT
    	$('#msel').multiSelect();
	// END MULTISELECT

});

