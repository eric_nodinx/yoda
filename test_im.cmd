.mode list
.import "../etl/orders.csv" orders
.import "../etl/markers.csv" mrkrequest
.import "../etl/details.csv" orderdetails
.import "../etl/customers.csv" customers
.import "../etl/proofs.csv" proofs
.import "../etl/items.csv" items

.mode csv
.import "../etl/categories.csv" categories
.import "../etl/item_types.csv" itemtypes 
.import "../etl/item_options.csv" itemoptions 
.import "../etl/marker status.csv" markerstatuses
.import "../etl/option_types.csv" optiontypes
.import "../etl/order_status_table.csv" orderstats 
.import "../etl/order_types.csv" ordertypes 
.import "../etl/printing time.csv" printtimes 
.import "../etl/priorities.csv" priorities 
.import "../etl/shipping types.csv" shiptypes 
.import "../etl/po_table.csv" po 
.import "../etl/podetail_table.csv" podetails
.import "../etl/users.csv" tg_user

.quit
